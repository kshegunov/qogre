#ifndef QOGREORIENTATION_H
#define QOGREORIENTATION_H

#include "qogre.h"

class QQuaternion;
class QVector3D;

class QOgreOrientationPrivate;

//! The class provides a common interface for manipulating orientations of the ogre objects. The
//! class provides an implicit cast to the QQuaternion. Objects couldn't be created directly, but
//! rather they should be retrieved from a ogre object with orientation and then manipulated.
class QOGRE_API QOgreOrientation
{
	QOGRE_PRIVATE(QOgreOrientation)
	QOGRE_DISABLE_COPY(QOgreOrientation)

public:
	//! The constructor. Initializes the object and prepares it for use.
	//! \param d The orientation's private object.
	QOgreOrientation(QOgreOrientationPrivate & d);

	//! Returns the orientation's object axis.
	//! \return The orientation's axis.
	QVector3D axis() const;

	//! Changes the orientation using a quaternion. The quaternion supplies the axis and angle for
	//! the new orientation.
	//! \param quaternion The new orientation.
	void change(const QQuaternion & quaternion);

	//! Changes the orientation using an axis and an angle.
	//! \param axis The new orientation's axis.
	//! \param angle The new orientation's angle.
	void change(const QVector3D & axis, qreal angle);

	//! Changes the orientation using an axis and an angle. The axis is given as vector coordinates.
	//! \param x The new orientation axis' x coordinate.
	//! \param y The new orientation axis' y coordinate.
	//! \param z The new orientation axis' z coordinate.
	//! \param angle The new orientation angle
	void change(qreal x, qreal y, qreal z, qreal angle);

	//! Rotates the current orientation vector using a quaternion.
	//! \param quaternion The rotation orientation.
	void rotate(const QQuaternion & quaternion);

	//! Rotates the current orientation vector using an axis and angle of rotation.
	//! \param angle The rotation angle.
	//! \param axis The rotation axis.
	void rotate(qreal angle, const QVector3D & axis);

	//! Rotates the current orientation vector using one of the priciple axes and an angle of rotation.
	//! \param angle The rotation angle.
	//! \param axis The rotation axis.
	void rotate(qreal angle, QtOgre::PrincipalAxes axis);

	//! Rotates the current orientation vector using one of the principle planes.
	//! The rotation axis is on the plane and is perpendicular to the position vector of the node.
	//! \param angle The rotation angle.
	//! \param plane The principle plane.
	void rotate(qreal angle, QtOgre::PrincipalPlanes plane);

	//! Rotates the current orientation vector using one of the principle planes.
	//! The rotation axis is on the plane and is perpendicular to the vector provided as argument.
	//! \param angle The rotation angle.
	//! \param vector The reference vector.
	//! \param plane The principle plane.
	void rotate(qreal angle, const QVector3D & vector, QtOgre::PrincipalPlanes plane);


	//! Resets the object's orientation to its initial state.
	void reset();

	//! Performs a yaw rotation.
	//! \param angle The rotation angle (in degrees).
	void yaw(qreal angle);

	//! Performs a pitch rotation
	//! \param angle The rotation angle (in degrees).
	void pitch(qreal angle);

	//! Performs a roll rotation
	//! \param angle The rotation angle (in degrees).
	void roll(qreal angle);

	//! Casts the orientation object to a quaternion. The operator allows the orientation to be
	//! treated as a quaternion in all contexts where it's necessary.
	operator QQuaternion () const;

private:
	QOgreOrientationPrivate * d_ptr;	//!<  A pointer to the orientation's private object (PIMPL idiom).
};

#endif // QOGREORIENTATION_H
