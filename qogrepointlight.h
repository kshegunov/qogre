#ifndef QOGREPOINTLIGHT_H
#define QOGREPOINTLIGHT_H

#include "qogrelight.h"

class QOgrePosition;
class QOgrePointLightPrivate;

//! This class represents a point light.
//! The point light emits in all possible directions with an equal intensity. Usefull for artificial
//! unoriented lights like light bulbs.
class QOGRE_API QOgrePointLight : public QOgreLight
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgrePointLight)
	QOGRE_OBJECT(QOgrePointLight)

public:
	//! The point light constructor.
	//! Creates and initializes the light.
	//! \sa QOgreLight::QOgreLight()
	//! \param scene The scene to which the light is attached.
	explicit QOgrePointLight(QOgreScene * scene);

	//! Retrieves the light's position object.
	//! Returns a reference to the light's position and allows manipulating it.
	//! \return The light's position.
	QOgrePosition & position();

	//! Retrieves the light's position object.
	//! Returns a reference to the light's position.
	//! \return The light's immutable position.
	const QOgrePosition & position() const;
};

#endif // QOGREPOINTLIGHT_H
