#ifndef QOGREDIRECTIONALLIGHT_H
#define QOGREDIRECTIONALLIGHT_H

#include "qogrelight.h"

class QVector3D;
class QOgreDirectionalLightPrivate;

//! This class represents a directional light.
//! A directional light is an uniform unidirectional homogenous light source, which has no fixed
//! position, but rather emits in the whole space. It hits every object on the scene under a fixed
//! angle with a fixed intensity. Usefull for natural outdoor lights as the sun and moon.
class QOGRE_API QOgreDirectionalLight : public QOgreLight
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreDirectionalLight)
	QOGRE_OBJECT(QOgreDirectionalLight)

public:
	//! The directional light constructor.
	//! Creates and initializes the light.
	//! \sa QOgreLight::QOgreLight()
	//! \param scene The scene to which the light is attached.
	explicit QOgreDirectionalLight(QOgreScene * scene);

	//! Gets the light's direction.
	//! \sa setDirection()
	//! \return The light's direction.
	QVector3D direction() const;

	//! Sets the light's direction.
	//! \sa direction()
	//! \param direction The light's direction.
	void setDirection(const QVector3D & direction);
};

#endif // QOGREDIRECTIONALLIGHT_H
