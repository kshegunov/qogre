#include "qogrepagemanager.h"
#include "qogrepagemanager_p.h"
#include "qogreworld.h"
#include "qogrecamera.h"
#include "qogrecast.h"

QOgrePageManager::QOgrePageManager(QOgreEngine * engine)
	: QOgreObject(*new QOgrePageManagerPrivate(this), engine)
{
}

void QOgrePageManager::addCamera(QOgreObject * object)
{
	QOGRE_D(QOgrePageManager);
	QOgreCamera * camera = qobject_cast<QOgreCamera *>(object);
	if (!camera)
		return;

	Ogre::Camera * ogreCamera = qogre_cast(camera);
	if (d->pageManager->hasCamera(ogreCamera))
		return;

	d->pageManager->addCamera(ogreCamera);

	QObject::connect(camera, SIGNAL(aboutToShutdown(QOgreObject *)), this, SLOT(removeCamera(QOgreObject *)));
}

void QOgrePageManager::removeCamera(QOgreObject * object)
{
	QOGRE_D(QOgrePageManager);
	QOgreCamera * camera = qobject_cast<QOgreCamera *>(object);
	if (!camera)
		return;

	Ogre::Camera * ogreCamera = qogre_cast(camera);
	if (d->pageManager->hasCamera(ogreCamera))
		return;

	d->pageManager->removeCamera(qogre_cast(camera));
}

