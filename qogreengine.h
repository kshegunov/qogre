#ifndef QOGREENGINE_H
#define QOGREENGINE_H

#include "qogre.h"
#include "qogreobject.h"

class QApplication;
class QOgrePageManager;
class QOgreRenderer;

//! The engine class manages the application's main settings and object lifetimes.
//! The class ensures the proper Ogre3D control flow, so that the initialization routines
//! are called in the proper order. It allows to set the plugin directories and the
//! resource locations (the actual resources are loaded automatically as needed).
class QOGRE_API QOgreEngine : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreEngine)
	QOGRE_OBJECT(QOgreEngine)

	typedef QtOgre::ResourceType ResourceType;

public:
	//! The ogre engine constructor.
	//! Initializes the the Ogre3D framework.
	//! Only one engine object should be constructed, and this should be done
	//! before using any other GUI objects - windows, meshes, scenes, etc.
	//! Requres a valid QApplication instance but does not inherit because of the debug-release linkage problem on windows.
	//! \param app The qt application object.
	QOgreEngine(QApplication & app);

	//! Gets a list of the currently registered plugin directories' paths.
	//! \return The list of registered plugin directories.
	//! \sa addPluginDirectory()
	QStringList pluginDirectories() const;

	//! Registers a path as a plugin-containing directory. The plugins are loaded automatically
	//! from the available directories. This function should be called before any other objects
	//! are created, otherwise the directory's plugins will not be loaded.
	//! \param path The directory path.
	//! \sa pluginDirectories()
	void addPluginDirectory(const QString & path);

	//! Registers a resource location. The resources are loaded automatically on demand by
	//! auto-matching the available locations. A resource location is dependent on the resource type
	//! and could be a filesystem path, a Qt resource path, a file. etc.
	//! \param location The resource location (usually a path-like string).
	//! \param type The resource type.
	//! \sa QtOgre::ResourceType
	void addResourceLocation(const QString & location, ResourceType type = QtOgre::FilesystemResource);

	//! Requests a plugin be loaded when intializing the engine
	//! \param plugin The plugin's file name
	//! \sa addPluginDirectory()
	void loadPlugin(const QString & plugin);

	//! Requests a set of plugin be loaded when intializing the engine
	//! \param plugins A list of plugin file names
	//! \sa addPluginDirectory()
	void loadPlugins(const QStringList & plugins);

	//! Retrieves the page manager object.
	//! \return The page manager.
	QOgrePageManager * pageManager();

	//! Retrieves the immutable page manager object.
	//! \return The page manager.
	const QOgrePageManager * pageManager() const;

	//! Gets the engine object's instance.
	//! \return A pointer to the engine's object, or NULL if it was not initialized.
	static QOgreEngine * instance();

public slots:
	//! Renders a frame and updates all related windows.
	void render();

private slots:
	//! Starts the ogre engine
	void start();

	//! Loads the requested resources.
	void loadAllResources();

signals:
	//! Emitted after the engine has started.
	void started();

	//! Emitted after the engine initialization has finished.
	void ready();

	//! Emitted after resources are loaded (may be done multiple times).
	void resourcesLoaded();

	//! Emitted after the plugins are loaded (if any).
	void pluginsLoaded();
};

#endif // QOGREENGINE_H
