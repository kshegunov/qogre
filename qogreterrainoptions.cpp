#include "qogreterrainoptions.h"
#include "qogreterrainoptions_p.h"
#include "qogrecast.h"

QOgreTerrainOptions::QOgreTerrainOptions()
	: d(new QOgreTerrainOptionsPrivate)
{
}

QOgreTerrainOptions::~QOgreTerrainOptions()
{
}

void QOgreTerrainOptions::setMaxPixelError(qreal error)
{
	d->options->setMaxPixelError(error);
}

qreal QOgreTerrainOptions::maxPixelError() const
{
	return d->options->getMaxPixelError();
}

void QOgreTerrainOptions::setCompositeMapDistance(qreal distance)
{
	d->options->setCompositeMapDistance(distance);
}

qreal QOgreTerrainOptions::compositeMapDistance() const
{
	return d->options->getCompositeMapDistance();
}

void QOgreTerrainOptions::setCompositeMapAmbientColor(const QColor & color)
{
	d->options->setCompositeMapAmbient(qogre_cast(color));
}

QColor QOgreTerrainOptions::compositeMapAmbientColor() const
{
	return qogre_cast(d->options->getCompositeMapAmbient());
}

void QOgreTerrainOptions::setCompositeMapDiffuseColor(const QColor & color)
{
	d->options->setCompositeMapDiffuse(qogre_cast(color));
}

QColor QOgreTerrainOptions::compositeMapDiffuseColor() const
{
	return qogre_cast(d->options->getCompositeMapDiffuse());
}

void QOgreTerrainOptions::setCompositeMapSize(quint16 size)
{
	d->options->setCompositeMapSize(size);
}

quint16 QOgreTerrainOptions::compositeMapSize() const
{
	return d->options->getCompositeMapSize();
}
