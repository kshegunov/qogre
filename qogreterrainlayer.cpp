#include "qogreterrainlayer.h"

#include <QList>
#include <QSharedData>

class QOGRE_LOCAL QOgreTerrainLayerPrivate : public QSharedData
{
	friend class QOgreTerrainLayer;

	QOgreTerrainLayerPrivate(qreal size, const QStringList & texturesList)
	{
		worldSize = size;
		textures = texturesList;
	}

	qreal worldSize;
	QList<QString> textures;
};

// ---------------------------------------------------------------------------------------------------- //

QOgreTerrainLayer::QOgreTerrainLayer(qreal worldSize, const QStringList & textures)
	: d(new QOgreTerrainLayerPrivate(worldSize, textures))
{
}

QOgreTerrainLayer::QOgreTerrainLayer(const QOgreTerrainLayer & other)
	: d(other.d)
{
}

QOgreTerrainLayer::~QOgreTerrainLayer()
{
}

QOgreTerrainLayer & QOgreTerrainLayer::operator = (const QOgreTerrainLayer & other)
{
	d = other.d;
	return *this;
}

qreal QOgreTerrainLayer::worldSize() const
{
	return d->worldSize;
}

void QOgreTerrainLayer::setWorldSize(qreal worldSize)
{
	d->worldSize = worldSize;
}

QStringList QOgreTerrainLayer::textures() const
{
	return d->textures;
}

void QOgreTerrainLayer::setTextures(const QStringList & textures)
{
	d->textures = textures;
}

void QOgreTerrainLayer::addTexture(const QString & texture)
{
	d->textures.append(texture);
}

void QOgreTerrainLayer::removeTexture(const QString & texture)
{
	d->textures.removeAll(texture);
}
