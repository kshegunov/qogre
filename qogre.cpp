#include "qogre.h"

#include <QVector3D>

const QVector3D QtOgre::UnitVectorX(1, 0, 0);
const QVector3D QtOgre::UnitVectorY(0, 1, 0);
const QVector3D QtOgre::UnitVectorZ(0, 0, 1);

const QVector3D & QtOgre::qPrincipleAxisVector(QtOgre::PrincipalAxes axis)
{
	switch (axis)
	{
	case QtOgre::XAxis:
		return QtOgre::UnitVectorX;
	case QtOgre::YAxis:
		return QtOgre::UnitVectorY;
	case QtOgre::ZAxis:
		return QtOgre::UnitVectorZ;
	}

	Q_ASSERT(false);
}

const QVector3D & QtOgre::qPrinciplePlaneVector(QtOgre::PrincipalPlanes plane)
{
	switch (plane)
	{
	case QtOgre::XYPlane:
		return QtOgre::UnitVectorZ;
	case QtOgre::XZPlane:
		return QtOgre::UnitVectorY;
	case QtOgre::YZPlane:
		return QtOgre::UnitVectorX;
	}

	Q_ASSERT(false);
}
