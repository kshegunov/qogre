#ifndef QOGREPOSITION_H
#define QOGREPOSITION_H

#include "qogre.h"

#include <QFlags>

//! \typedef typedef QFlags<QtOgre::PositionTypes> PositionType
//! Defines the possible translations which could be performed on a position.
Q_DECLARE_FLAGS(PositionType, QtOgre::PositionTypes)
Q_DECLARE_OPERATORS_FOR_FLAGS(PositionType)

class QVector3D;

class QOgrePositionPrivate;

//! The class provides a common interface for manipulating positions of the ogre objects. The
//! class provides an implicit cast to the QVector3D. Objects couldn't be created directly, but
//! rather they should be retrieved from a ogre object with position and then manipulated.
class QOGRE_API QOgrePosition
{
	QOGRE_PRIVATE(QOgrePosition)
	QOGRE_DISABLE_COPY(QOgrePosition)

public:
	//! The constructor. Initializes the object and prepares it for use.
	//! \param d The position's private object.
	QOgrePosition(QOgrePositionPrivate & d);

	//! Changes the position of an object using a supplied vector. The vector is providing the new
	//! position's coordinates.
	//! \param vector The new position.
	void change(const QVector3D & vector);

	//! Changes the position of an object using a set of coordinates. The coordinates fully
	//! determine the new position.
	//! \param x The new position's x coordinate.
	//! \param y The new position's y coordinate.
	//! \param z The new position's z coordinate.
	void change(qreal x, qreal y, qreal z);

	//! Performs a translation on the current position. The translation's origin coordinate system
	//! is determined by the position type supplied. The origin coordinate system could be fixed to
	//! the global coordinate space (QtOgre::GlobalPosition), the parent's coordinate space
	//! (QtOgre::AbsolutePosition | QtOgre::LocalPosition), which is the default, or to the current
	//! object's coordinate space (QtOgre::RelativePosition | QtOgre::LocalPosition).
	//! \param vector The translation vector.
	//! \param position The position type.
	void move(const QVector3D & vector, PositionType position = QtOgre::AbsolutePosition | QtOgre::LocalPosition);

	//! Performs a translation on the current position. The translation's origin coordinate system
	//! is determined by the position type supplied. The origin coordinate system could be fixed to
	//! the global coordinate space (QtOgre::GlobalPosition), the parent's coordinate space
	//! (QtOgre::AbsolutePosition | QtOgre::LocalPosition), which is the default, or to the current
	//! object's coordinate space (QtOgre::RelativePosition | QtOgre::LocalPosition).
	//! \param x The translation vector's x coordinate.
	//! \param y The translation vector's y coordinate.
	//! \param z The translation vector's z coordinate.
	//! \param position The position type.
	void move(qreal x, qreal y, qreal z, PositionType position = QtOgre::AbsolutePosition | QtOgre::LocalPosition);

	//! Casts the position object to a vector. The operator allows the position to be treated as a
	//! vector in all contexts where it's necessary.
	operator QVector3D () const;

private:
	QOgrePositionPrivate * d_ptr;	//!<  A pointer to the position's private object (PIMPL idiom).
};

#endif // QOGREPOSITION_H
