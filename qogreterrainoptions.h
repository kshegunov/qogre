#ifndef QOGRETERRAINOPTIONS_H
#define QOGRETERRAINOPTIONS_H

#include "qogre.h"
#include "qscopedpointer.h"

class QColor;
class QOgreTerrain;
class QOgreTerrainPrivate;

class QOgreTerrainOptionsPrivate;
class QOGRE_API QOgreTerrainOptions
{
	friend class QOgreTerrain;
	friend class QOgreTerrainPrivate;

	QOGRE_DISABLE_COPY(QOgreTerrainOptions)

	QOgreTerrainOptions();
	~QOgreTerrainOptions();

public:
	void setMaxPixelError(qreal);
	qreal maxPixelError() const;

	void setCompositeMapDistance(qreal);
	qreal compositeMapDistance() const;

	void setCompositeMapAmbientColor(const QColor &);
	QColor compositeMapAmbientColor() const;

	void setCompositeMapDiffuseColor(const QColor &);
	QColor compositeMapDiffuseColor() const;

	void setCompositeMapSize(quint16);
	quint16 compositeMapSize() const;

private:
	QScopedPointer<QOgreTerrainOptionsPrivate> d;
};

#endif // QOGRETERRAINOPTIONS_H
