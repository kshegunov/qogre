#include "qogreterraintile.h"
#include "qogreterraintile_p.h"
#include "qogrescene.h"
#include "qogrecast.h"

QOgreTerrainTile::QOgreTerrainTile(QOgreTerrain * terrain)
	: QOgreObject(*new QOgreTerrainTilePrivate(this, terrain), terrain)
{
}

QOgreTerrainTile::~QOgreTerrainTile()
{
}

void QOgreTerrainTile::load()
{
	QOGRE_D(QOgreTerrainTile);
	if (d->tile)
		return;
/*
	qint32 x = d->position.x(), y = d->position.y();

	Ogre::TerrainGroup * terrainGroup = qogre_cast(d->terrain);
	terrainGroup->loadTerrain(x, y, true);
	d->tile = terrainGroup->getTerrain(x, y);*/
}

bool QOgreTerrainTile::isLoaded() const
{
	return d()->terrain;
}

QPoint QOgreTerrainTile::position() const
{
	return d()->position;
}
