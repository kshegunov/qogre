#include "qogrenode.h"
#include "qogrenode_p.h"
#include "qogrecast.h"
#include "qogreworld.h"

QOgreNode::QOgreNode(QOgreWorld * world)
	: QOgreObject(*new QOgreNodePrivate(world->scene(), this), world->scene())
{
}

QOgreNode::QOgreNode(QOgreScene * scene)
	: QOgreObject(*new QOgreNodePrivate(scene, this), scene)
{
}

QOgreNode::QOgreNode(QOgreNode * parent)
	: QOgreObject(*new QOgreNodePrivate(parent, this), parent)
{
}

QOgreNode::QOgreNode(QOgreNodePrivate & d, QOgreObject * parent)
	: QOgreObject(d, parent)
{
}

QOgreNode::~QOgreNode()
{
}

QOgrePosition & QOgreNode::position()
{
	return d()->position;
}

const QOgrePosition & QOgreNode::position() const
{
	return d()->position;
}

QOgreOrientation & QOgreNode::orientation()
{
	return d()->orientation;
}

const QOgreOrientation & QOgreNode::orientation() const
{
	return d()->orientation;
}


QOgreNode * QOgreNode::parent()
{
	return dynamic_cast<QOgreNode *>(QObject::parent());
}

const QOgreNode * QOgreNode::parent() const
{
	return dynamic_cast<const QOgreNode *>(QObject::parent());
}

QOgreScene * QOgreNode::scene()
{
	QObject * parent = QObject::parent();
	QOgreScene * scene = dynamic_cast<QOgreScene *>(parent);
	return scene ? scene : dynamic_cast<QOgreNode *>(parent)->scene();
}

const QOgreScene * QOgreNode::scene() const
{
	QObject * parent = QObject::parent();
	QOgreScene * scene = dynamic_cast<QOgreScene *>(parent);
	return scene ? scene : dynamic_cast<QOgreNode *>(parent)->scene();
}
