#include "qogrerenderer.h"
#include "qogrerenderer_p.h"
#include "qogreengine.h"
#include "qogrecast.h"

#include <QApplication>
#include <QList>
#include <QVector>

QOgreRenderer::QOgreRenderer(const QString & name)
	: QOgreObject(*new QOgreRendererPrivate(name, this), QOgreEngine::instance())
{
}

QString QOgreRenderer::name() const
{
	return d()->name;
}

bool QOgreRenderer::isSelected() const
{
	return QOgreRendererPrivate::current() == this;
}

void QOgreRenderer::select()
{
	d()->select();
}

bool QOgreRenderer::isModified() const
{
	return d()->changed;
}

void QOgreRenderer::setResolution(qint32 width, qint32 height)
{
	d()->renderer->setConfigOption("Video Mode", QString("%1x%2").arg(width).arg(height).toStdString());
}

void QOgreRenderer::enable(RenderOptions options)
{
	QOGRE_D(QOgreRenderer);
	if ((d->options | options) != d->options)  {
		d->options |= options;
		d->changed = QOgreRendererPrivate::current() == this;
	}
}

void QOgreRenderer::disable(RenderOptions options)
{
	QOGRE_D(QOgreRenderer);
	if ((d->options & ~options) != d->options)  {
		d->options &= ~options;
		d->changed = QOgreRendererPrivate::current() == this;
	}
}

bool QOgreRenderer::isEnabled(RenderOptions options) const
{
	return (d()->options & options) == options;
}

bool QOgreRenderer::hasFeature(QtOgre::RenderFeatures features) const
{
	return qogre_cast(this)->getCapabilities()->hasCapability(qogre_cast(features));
}

QOgreRenderer * QOgreRenderer::renderer(const QString & name)
{
	return QOgreRendererPrivate::renderers.value(name, NULL);
}

QOgreRendererList QOgreRenderer::renderers()
{
	return QOgreRendererPrivate::renderers.values();
}

QOgreRenderer * QOgreRenderer::active()
{
	return QOgreRendererPrivate::current();
}
