#ifndef QOGREWORLD_H
#define QOGREWORLD_H

#include "qogreobject.h"

#include <QString>

class QOgrePageManager;
class QOgreScene;
class QOgreTerrain;

class QOGRE_API QOgreWorld : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreWorld)
	QOGRE_OBJECT(QOgreWorld)

public:
	QOgreWorld(const QString & name = QString());
	virtual ~QOgreWorld();

	void setScene(QOgreScene *);
	QOgreScene * scene();
	const QOgreScene * scene() const;

	void setTerrain(QOgreTerrain *);
	QOgreTerrain * terrain();
	const QOgreTerrain * terrain() const;

	void setName(const QString &);
	QString name() const;

	static QOgreWorld * world(const QString & name = QString());
};

#endif // QOGREWORLD_H
