#include "qogrespotlight.h"
#include "qogrespotlight_p.h"
#include "qogrecast.h"

#include <OgreMath.h>

QOgreSpotLight::QOgreSpotLight(QOgreScene * scene)
	: QOgreLight(*new QOgreSpotLightPrivate(qogre_cast(scene), qogre_cast(scene)->getRootSceneNode(), this), scene)
{
	d()->light->setType(Ogre::Light::LT_SPOTLIGHT);
}

QOgrePosition & QOgreSpotLight::position()
{
	return d()->position;
}

const QOgrePosition & QOgreSpotLight::position() const
{
	return d()->position;
}

QOgreOrientation & QOgreSpotLight::orientation()
{
	return d()->orientation;
}

const QOgreOrientation & QOgreSpotLight::orientation() const
{
	return d()->orientation;
}

qreal QOgreSpotLight::falloff() const
{
	return d()->light->getSpotlightFalloff();
}

void QOgreSpotLight::setFalloff(qreal falloff)
{
	d()->light->setSpotlightFalloff(falloff);
}

qreal QOgreSpotLight::innerAngle() const
{
	return Ogre::Radian(d()->light->getSpotlightInnerAngle()).valueDegrees();
}

void QOgreSpotLight::setInnerAngle(qreal angle)
{
	d()->light->setSpotlightInnerAngle(Ogre::Degree(angle));
}

qreal QOgreSpotLight::outerAngle() const
{
	return Ogre::Radian(d()->light->getSpotlightOuterAngle()).valueDegrees();
}

void QOgreSpotLight::setOuterAngle(qreal angle)
{
	d()->light->setSpotlightOuterAngle(Ogre::Degree(angle));
}

qreal QOgreSpotLight::nearClipDistance() const
{
	return d()->light->getSpotlightNearClipDistance();
}

void QOgreSpotLight::setNearClipDistance(qreal distance)
{
	d()->light->setSpotlightNearClipDistance(distance);
}
