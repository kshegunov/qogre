#ifndef QOGRERENDERER_H
#define QOGRERENDERER_H

#include "qogre.h"
#include "qogreobject.h"

class QString;

class QOgreRenderer;
class QOgreRendererPrivate;

//! \typedef QList<QOgreRenderer *> QOgreRendererList
//! A list of QOgreRenderer objects.
typedef QList<QOgreRenderer *> QOgreRendererList;

//! \typedef QFlags<QtOgre::RenderOptions> RenderOption
//! The options used to fine-tune the renderer behaviour.
Q_DECLARE_FLAGS(RenderOptions, QtOgre::RenderOption)
Q_DECLARE_OPERATORS_FOR_FLAGS(RenderOptions)

//! The class encapsulates an available render system. The render systems are provided by loading
//! the an Ogre3D plugin and are manipulated through this class. The render objects are used
//! internally and can't be created or destroyed, their lifetime is managed by the library itself.
class QOGRE_API QOgreRenderer : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreRenderer)
	QOGRE_OBJECT(QOgreRenderer)

private:
	//! The constructor. Creates the object and initilizes it for use.
	//! \param name The render system's name.
	explicit QOgreRenderer(const QString & name);

public:
	//! Gets the render system's name.
	//! \return The render system's name.
	QString name() const;

	//! Checks if this is the currently selected render system.
	//! \return Whether this is the currently selected render system.
	bool isSelected() const;

	//! Selects the render system for use. This function will (re)initialize the render system if
	//! needed.
	void select();

	//! Checks if the renderer's options were modified after the renderer was selected as the current rendering system.
	//! \return Whether the renderer options were modified.
	bool isModified() const;

	//! Sets the rendering resolution.
	//! \param width The width.
	//! \param height The height.
	void setResolution(qint32 width, qint32 height);

	//! Enables a set of rendering options.
	//! \param options The rendering options.
	void enable(RenderOptions options);

	//! Disables a set of rendering options.
	//! \param options The rendering options.
	void disable(RenderOptions options);

	//! Checks if a set of rendering options is enabled. This function will return true only if all
	//! the rendering options are set.
	//! \param options The rendering options to test for.
	//! \return Whether the rendering options supplied are set.
	bool isEnabled(RenderOptions options) const;

	//! Checks if a set of rendering features is supported.
	//! \param features The rendering features to test for.
	//! \return Whether the rendering features supplied are supported.
	bool hasFeature(QtOgre::RenderFeatures features) const;

	//! Retrieves a renderer object by it's name. The names are generated automatically by the
	//! Ogre3D plugins supplying the render systems.
	//! \param name The render system's name.
	//! \return The renderer object.
	static QOgreRenderer * renderer(const QString & name);

	//! Gets the list of all currently available renderers.
	//! \return A list of the currently available renderers.
	static QOgreRendererList renderers();

	//! Retrieves the active renderer.
	//! \return The renderer object.
	static QOgreRenderer * active();
};

#endif // QOGRERENDERER_H
