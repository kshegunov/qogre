#include "qogrepointlight.h"
#include "qogrepointlight_p.h"
#include "qogrecast.h"

QOgrePointLight::QOgrePointLight(QOgreScene * scene)
	: QOgreLight(*new QOgrePointLightPrivate(qogre_cast(scene), qogre_cast(scene)->getRootSceneNode(), this), scene)
{
	d()->light->setType(Ogre::Light::LT_POINT);
}

QOgrePosition & QOgrePointLight::position()
{
	return d()->position;
}

const QOgrePosition & QOgrePointLight::position() const
{
	return d()->position;
}
