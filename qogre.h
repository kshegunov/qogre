#ifndef QOGRE_H
#define QOGRE_H

#include <QtGlobal>
#include <Qt>

#ifdef QOGRE_LIBRARY
	#define QOGRE_API Q_DECL_EXPORT		//!< The export/import macro (relevant only for windows).
	#define QOGRE_LOCAL Q_DECL_HIDDEN	//!< This hides the symbol (relevant for linux only)

	//! Declares the public class for PIMPL.
	#define QOGRE_PRIVATE(Class) \
		friend class Class##Private; \
		inline Class##Private * d() { return reinterpret_cast<Class##Private *>(internal::getPtrHelper(d_ptr)); } \
		inline const Class##Private * d() const { return reinterpret_cast<const Class##Private *>(internal::getPtrHelper(d_ptr)); }

	//! Declares the private class for PIMPL.
	#define QOGRE_PUBLIC(Class) \
		friend class Class; \
		inline Class * q() { return reinterpret_cast<Class *>(q_ptr); } \
		inline const Class * q() const { return reinterpret_cast<const Class *>(q_ptr); }

	class QOgreCast;		//!< The ogre_cast provider.

	//! Declares a PIMPL compatible class and allowing for ogre_cast.
	#define QOGRE_OBJECT(Class) \
		QOGRE_PRIVATE(Class) \
		friend class QOgreCast;

	//! Declares a PIMPL compatible private class and allowing for ogre_cast.
	#define QOGRE_OBJECT_PRIVATE(Class) \
		QOGRE_PUBLIC(Class); \
		friend class QOgreCast;

	//! Declares the fast d-pointer expansion macro.
	#define QOGRE_D(Class) \
		Class##Private * const d(this->d())

	//! Declares the fast q-pointer expansion macro.
	#define QOGRE_Q(Class) \
		Class * const q(this->q())

	//! Explicitly instantiates a template.
	#define QOGRE_INSTANTIATE(Template) template class Template

	namespace internal
	{
		// Helpers for the PIMPL idiom
		template <typename T> static inline T * getPtrHelper(T * ptr) { return ptr; }
		template <typename Wrapper> static inline typename Wrapper::pointer getPtrHelper(Wrapper & p)  { return p.data(); }
		template <typename Wrapper> static inline typename Wrapper::pointer getPtrHelper(const Wrapper & p)  { return const_cast<typename Wrapper::pointer>(p.data()); }
	}
#else
	#define QOGRE_API Q_DECL_IMPORT
	#define QOGRE_LOCAL

	#define QOGRE_PRIVATE(Class)
	#define QOGRE_OBJECT(Class)
#endif

//! Exports a template specialization.
#ifdef Q_OS_WIN
	#define QOGRE_EXPORT_TEMPLATE(Template) template class QOGRE_API Template
#else
	#define QOGRE_EXPORT_TEMPLATE(Template) extern template class Template
#endif

//! Disables copying the object.
#define QOGRE_DISABLE_COPY Q_DISABLE_COPY

class QVector3D;
namespace QtOgre
{
	//! Describes the supported resource types.
	enum ResourceType { FilesystemResource, ZipResource, QtResource, UnknownResource };
	//! Describes the supported shadow types.
	enum ShadowType { ModulativeTextureShadow, ModulativeStencilShadow, AdditiveStencilShadow, NoShadow };
	//! Flags describing the position.
	enum PositionTypes  {
		GlobalPosition = 0x01,
		LocalPosition = 0x02,
		AbsolutePosition = 0x04,
		RelativePosition = 0x08
	};
	//! Render options
	enum RenderOption  {
		RenderFullScreen = 0x01,
		RenderVSync = 0x02
	};
	//! Render capabilities
	enum RenderFeatures  {
		InfiniteFarPlaneFeature = 0x01
	};

	enum PrincipalAxes  {
		XAxis = Qt::XAxis,
		YAxis = Qt::YAxis,
		ZAxis = Qt::ZAxis
	};

	enum PrincipalPlanes  {
		XYPlane, XZPlane, YZPlane
	};

	QOGRE_API extern const QVector3D UnitVectorX;
	QOGRE_API extern const QVector3D UnitVectorY;
	QOGRE_API extern const QVector3D UnitVectorZ;

	QOGRE_API const QVector3D & qPrincipleAxisVector(QtOgre::PrincipalAxes);
	QOGRE_API const QVector3D & qPrinciplePlaneVector(QtOgre::PrincipalPlanes);
}

#endif // QOGRE_H
