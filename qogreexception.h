#ifndef QOGREEXCEPTION_H
#define QOGREEXCEPTION_H

#include "qogre.h"

#include <QString>

//! The QOgreException class provides the base exception class within the library.
//! Any error could be caught as a QOgreException object, but it's much better to subclass it
//! and introduce the different error types as a new class.
class QOGRE_API QOgreException
{
protected:
	//! The exception constructor. Sets the exception text and initializes the object.
	//! \param description The error description.
	QOgreException(const QString & description);

public:
	//! Gets a description of the error. This function will never throw an exception.
	//! If there is a more complex rule by which the error text is constructed, this function
	//! could be overridden but it should be kept as a no-throw.
	//! \return The error's description.
	virtual const QString & description() const throw();

private:
	QString text;	//!< The exception text.
};

//! This class represents an initialization exception. It is thrown when the Ogre3D engine failed to
//! initialize for some reason.
class QOGRE_API QOgreInitializationException : public QOgreException
{
public:
	//! The exception constructor. Sets the exception text and initializes the object.
	//! \param text The error description.
	QOgreInitializationException(const QString & text);
};

#endif // QOGREEXCEPTION_H
