#ifndef QOGREWIDGET_H
#define QOGREWIDGET_H

#include "qogre.h"
#include "qogrescene.h"

#include <QWidget>

class QOgreCamera;
class QOgreScene;
class QOgreWidgetPrivate;

//! The class is representing a window on which rendering is done.
//! The widget automatically creates its own camera and viewport and needs only to be given a scene
//! which to display.
class QOGRE_API QOgreWidget : public QWidget
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreWidget)
	QOGRE_OBJECT(QOgreWidget)

public:
	//! The widget constructor. Creates the object and prepares it for use. Note that the actual
	//! window is not created in the constructor, so any window-modifying calls should be deferred
	//! to a later time.
	//! \param parent The widget's parent window.
	//! \param flags The window flags.
	explicit QOgreWidget(QWidget * parent = NULL, Qt::WindowFlags flags = 0);

	//! The destructor. Destroys the window and deletes the private object. Frees any allocated or
	//! acquired resources.
	virtual ~QOgreWidget();

	//! Checks if the widget was created and initialized properly.
	//! \return Whether the widget is valid.
	bool isValid() const;

	//! Gets a reference to the widget's camera.
	//! \return The widget's camera.
	QOgreCamera * camera();

	//! Gets a reference to the immutable widget's camera.
	//! \return The widget's camera.
	const QOgreCamera * camera() const;

	//! Sets the widget's camera.
	//! \param camera The widget's camera.
	void setCamera(QOgreCamera *);

	//! Sets the widget's background color. The background color is used when there is no object to
	//! render on a given portion of the screen.
	//! \sa backgroundColor()
	//! \param color The background color.
	void setBackgroundColor(const QColor & color);

	//! Gets the widget's background color. The background color is used when there is no object to
	//! render on a given portion of the screen.
	//! \sa setBackgroundColor()
	//! \return The background color.
	QColor backgroundColor() const;

	//! Enables the widget to render automatically.
	//! \sa isAutoUpdated()
	//! \param enable Whether auto update should be enabled.
	void enableAutoUpdate(bool enable = true);

	//! Enables the widget to render automatically.
	//! \sa isAutoUpdated()
	//! \param disable Whether auto update should be disabled.
	void disableAutoUpdate(bool disable = true);

	//! Checks if the widget renders automatically.
	//! If the widget is not set to automatically render you should call QOgreEngine::render() to update its contents.
	//! \sa QOgreEngine::render()
	//! \return Whether auto update is enabled.
	bool isAutoUpdated() const;

	//! Gets the widget's paint engine. This is reimplementation to disallow painting with the Qt's
	//! QPainter objects, because the results are unreliable.
	//! \sa QWidget::paintEngine()
	//! \return The widget's paint engine.
	virtual QPaintEngine * paintEngine() const;

signals:
	//! This signal is emitted when the widget is ready to be used. The widget will signal this when
	//! the graphics context is created and initialized, the native windows are created and the
	//! widget has attached to them.
	//! \param widget The ready widget
	void ready(QOgreWidget * widget);

	//! This signal is emitted when the camera attached to the widget is changed.
	//! \param widget The new camera.
	void cameraChanged(QOgreCamera * camera);

	//! This signal is emitted when the widget is about to be destroyed.
	void aboutToShutdown();

	//! This signal is emitted before the window contents are rendered.
	void renderingStarted();

	//! This signal is emitted after the window contents are rendered.
	void renderingFinished();


private slots:
	//! Initializes the window.
	void setup();

	//! Destroys the window and releases the acquired resources. After calling this slot the widget
	//! will no longer be valid.
	void shutdown();

protected:
	//! Handles the close events.
	//! \sa QWidget::closeEvent()
	//! \param event The close event.
	virtual void closeEvent(QCloseEvent * event);

	//! Handles the widget's paint events.
	//! \sa QWidget::paintEvent()
	//! \param event The paint event.
	virtual void paintEvent(QPaintEvent * event);

	//! Handles the widget's resize events.
	//! \sa QWidget::resizeEvent()
	//! \param event The resize event.
	virtual void resizeEvent(QResizeEvent * event);

private:
	QOgreWidgetPrivate * d_ptr;		//!<  A pointer to the widget's private object (PIMPL idiom).
};

#endif // QOGREWIDGET_H
