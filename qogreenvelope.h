#ifndef QOGREENVELOPE_H
#define QOGREENVELOPE_H

#include "qogre.h"
#include "qogresize.h"

#include <QTypeInfo>
#include <QVector3D>

//! This class represents a 3D object's envelope.
//! The envelope is the box which fully encloses the 3D object. Every visible object have an
//! envelope, which can be used to find the extremal dimensions of the object, or the geometrical
//! center. The envelope is always aligned along the main x, y and z axes.
class QOGRE_API QOgreEnvelope
{
public:
	//! The default constructor.
	//! Creates an empty envelope.
	QOgreEnvelope();

	//! The envelope constructor.
	//! Creates an envelope from a center point and a size.
	//! \param center The envelope's center point.
	//! \param size The envelope's size.
	QOgreEnvelope(const QVector3D & center, const QOgreSize & size);

	//! Checks if the envelope is valid.
	//! If one of the envelope's dimensions is equal to zero, then it's considered invalid.
	//! \return Whether the envelope is valid.
	bool isValid() const;

	//! Gets the envelope's center.
	//! \sa setCenter()
	//! \return The envelope's center.
	QVector3D center() const;

	//! Sets the envelope's center point.
	//! \sa center()
	//! \param center The envelope's center.
	void setCenter(const QVector3D & center);

	//! Gets the envelope's size.
	//! \sa setSize()
	//! \return The envelope's size.
	QOgreSize size() const;

	//! Sets the envelope's size.
	//! \sa size()
	//! \param size The envelope's size.
	void setSize(const QOgreSize & size);

private:
	QVector3D ctr;		//!< The envelope's center point.
	QOgreSize sz;		//!< The envelope's size.
};
Q_DECLARE_TYPEINFO(QOgreEnvelope, Q_MOVABLE_TYPE);		// Tell Qt it may use memcpy/memmove for this object


// --------------------------------------------------------------------------------------- //
// --- QOgreEnvelope implementation ------------------------------------------------------ //
// --------------------------------------------------------------------------------------- //

inline QOgreEnvelope::QOgreEnvelope()
{
}

inline QOgreEnvelope::QOgreEnvelope(const QVector3D & center, const QOgreSize & size)
	: ctr(center), sz(size)
{
}

inline bool QOgreEnvelope::isValid() const
{
	return sz.isValid();
}

inline QVector3D QOgreEnvelope::center() const
{
	return ctr;
}

inline void QOgreEnvelope::setCenter(const QVector3D & center)
{
	ctr = center;
}

inline QOgreSize QOgreEnvelope::size() const
{
	return sz;
}

inline void QOgreEnvelope::setSize(const QOgreSize & size)
{
	sz = size;
}

#endif // QOGREENVELOPE_H
