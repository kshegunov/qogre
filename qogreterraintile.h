#ifndef QOGRETERRAINTILE_H
#define QOGRETERRAINTILE_H

#include "qogreobject.h"

class QOgreTerrain;

class QOgreTerrainTilePrivate;
class QOGRE_API QOgreTerrainTile : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreTerrainTile)
	QOGRE_OBJECT(QOgreTerrainTile)

	friend class QOgreTerrain;

	QOgreTerrainTile(QOgreTerrain *);

public:
	~QOgreTerrainTile();

	void load();
	bool isLoaded() const;

	QPoint position() const;
};

#endif // QOGRETERRAINTILE_H
