#include "qogredirectionallight.h"
#include "qogredirectionallight_p.h"
#include "qogrecast.h"

QOgreDirectionalLight::QOgreDirectionalLight(QOgreScene * scene)
	: QOgreLight(*new QOgreDirectionalLightPrivate(qogre_cast(scene), qogre_cast(scene)->getRootSceneNode(), this), scene)
{
	d()->light->setType(Ogre::Light::LT_DIRECTIONAL);
}

QVector3D QOgreDirectionalLight::direction() const
{
	return qogre_cast(d()->light->getDirection());
}

void QOgreDirectionalLight::setDirection(const QVector3D & direction)
{
	d()->light->setDirection(qogre_cast(direction));
}
