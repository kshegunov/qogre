#include "qogreengine.h"
#include "qogreengine_p.h"
#include "qogrerenderer.h"
#include "qogrecast.h"

#include <QDir>
#include <QApplication>

static QOgreEngine * engine = NULL;

QOgreEngine::QOgreEngine(QApplication & app)
	: QOgreObject(*new QOgreEnginePrivate(this), &app)
{
	engine = this;

	QObject::connect(&app, SIGNAL(aboutToQuit()), this, SLOT(shutdown()));
	QMetaObject::invokeMethod(this, "start", Qt::QueuedConnection);
}

QStringList QOgreEngine::pluginDirectories() const
{
	return QDir::searchPaths(QOgreEnginePrivate::PLUGINS_PREFIX);
}

void QOgreEngine::addPluginDirectory(const QString & path)
{
	QDir::addSearchPath(QOgreEnginePrivate::PLUGINS_PREFIX, path);
}

void QOgreEngine::addResourceLocation(const QString & location, ResourceType type)
{
	Q_ASSERT(type >= 0 && type < QtOgre::UnknownResource);
	d()->addResourceLocation(location, type);
}

void QOgreEngine::loadPlugin(const QString & plugin)
{
	d()->requiredPlugins.insert(plugin);
}

void QOgreEngine::loadPlugins(const QStringList & plugins)
{
	foreach (const QString plugin, plugins)
		loadPlugin(plugin);
}

QOgrePageManager * QOgreEngine::pageManager()
{
	return d()->pageManager;
}

const QOgrePageManager * QOgreEngine::pageManager() const
{
	return d()->pageManager;
}


QOgreEngine * QOgreEngine::instance()
{
	return engine;
}

void QOgreEngine::render()
{
	d()->root->renderOneFrame();
}

void QOgreEngine::start()
{
	d()->initialize();
	emit started();
}

void QOgreEngine::loadAllResources()
{
	if (d()->loadAllResources())
		emit ready();
}
