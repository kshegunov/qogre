#ifndef QOGRELIGHT_H
#define QOGRELIGHT_H

#include "qogre.h"
#include "qogreobject.h"
#include "qogrescene.h"

class QColor;
class QOgreLightPrivate;

//! This is the base for all supported lights.
//! No objects could be created directly from this class, but it provides the common API for
//! the other light types.
class QOGRE_API QOgreLight : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreLight)
	QOGRE_OBJECT(QOgreLight)

protected:
	//! The light constructor.
	//! Creates and initializes the light, acquires the needed resources.
	//! \param d The private object to use.
	//! \param parent The scene to which the light is attached.
	QOgreLight(QOgreLightPrivate & d, QOgreScene * parent);

public:
	//! The light destructor.
	//! Deletes the light and frees all acquired resources.
	virtual ~QOgreLight();

	//! Checks if the light was created and properly initialized.
	//! \return Whether the light is valid.
	bool isValid() const;

	//! Gets the light's diffuse color.
	//! The diffuse color is used to render the light's diffuse reflections. Diffuse reflections
	//! have lower lumnous intensity, but the light is scattered in all directions (assuming a fixed
	//! angle incident light ray).
	//! \sa setDiffuseColor()
	//! \return The diffuse color.
	QColor diffuseColor() const;

	//! Sets the light's diffuse color.
	//! The diffuse color is used to render the light's diffuse reflections. Diffuse reflections
	//! have lower lumnous intensity, but the light is scattered in all directions (assuming a fixed
	//! angle incident light ray).
	//! \sa diffuseColor()
	//! \param color The diffuse color.
	void setDiffuseColor(const QColor & color);

	//! Gets the light's specular color.
	//! The specular color is used to render the light's specular reflections. Specular reflections
	//! have higher lumnous intensity, but the light is scattered in one direction (assuming a fixed
	//! angle incident light ray).
	//! \sa setSpecularColor()
	//! \return The specular color.
	QColor specularColor() const;

	//! Sets the light's specular color.
	//! The specular color is used to render the light's specular reflections. Specular reflections
	//! have higher lumnous intensity, but the light is scattered in one direction (assuming a fixed
	//! angle incident light ray).
	//! \sa specularColor()
	//! \param color The specular color.
	void setSpecularColor(const QColor & color);
};

#endif // QOGRELIGHT_H
