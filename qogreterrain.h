#ifndef QOGRETERRAIN_H
#define QOGRETERRAIN_H

#include "qogreobject.h"

class QOgreScene;
class QOgreLight;
class QOgreTerrainLayer;
class QOgreTerrainOptions;
class QOgreTerrainTile;
class QOgreWorld;

class QOGRE_API QOgreTerrain : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreTerrain)
	QOGRE_OBJECT(QOgreTerrain)

public:
	QOgreTerrain(QOgreScene *);

	QOgreScene * scene();
	const QOgreScene * scene() const;

	QOgreTerrainOptions & options();
	const QOgreTerrainOptions & options() const;

	void setAlignment(QtOgre::PrincipalPlanes);
	QtOgre::PrincipalPlanes alignment() const;

	void setTileSize(quint16);
	quint16 tileSize() const;

	void setSize(QSize);
	void setSize(qint32, qint32);
	QSize size() const;

	void setWorldSize(qreal);
	qreal worldSize() const;

	void setOrigin(const QVector3D &);
	QVector3D origin() const;

	void setLevelsOfDetail(qint8);
	qint8 levelsOfDetail() const;

	void setLight(QOgreLight *);
	QOgreLight * light();
	const QOgreLight * light() const;

	QOgreTerrainTile & tile(QPoint position);
	const QOgreTerrainTile & tile(QPoint position) const;
	QOgreTerrainTile & tile(qint32 x, qint32 y);
	const QOgreTerrainTile & tile(qint32 x, qint32 y) const;

	void addLayer(const QOgreTerrainLayer &);

	void setWorld(QOgreWorld *);
	QOgreWorld * world();
	const QOgreWorld * world() const;

	void setPageSize(QSize);
	void setPageSize(qint32, qint32);
	QSize pageSize();

signals:
	void initialized();

public slots:
	void initialize();

	void update();
};

#endif // QOGRETERRAIN_H
