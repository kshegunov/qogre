#ifndef QOGRESCENE_H
#define QOGRESCENE_H

#include "qogreobject.h"
#include "qogreworld.h"

#include <QFlags>
#include <QString>

class QColor;
class QOgreNode;

class QOgreScenePrivate;

//! The class is a base for all supported scene types. The scene is a collection of objects which
//! could be displayed or manipulated (orientations, positions).
class QOGRE_API QOgreScene : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreScene)
	QOGRE_OBJECT(QOgreScene)

protected:
	//! The constructor. Creates the scene and initializes the private object. This is supplied to
	//! allow the PIMPL idiom for derived classes.
	explicit QOgreScene(QOgreWorld *, QOgreScenePrivate &);

public:
	typedef QtOgre::ShadowType ShadowType;

	//! Retrieves the world object this scene is attached to.
	//! \return The world containing the current scene.
	QOgreWorld * world();

	//! Retrieves the world object this scene is attached to.
	//! \return The immutable world containing the current scene.
	const QOgreWorld * world() const;

	//! Checks if the scene was created and properly initialized.
	//! \return Whether the scene is valid.
	bool isValid() const;

	//! Gets the type of shadows used for this scene. There are several basic types from which a
	//! selection could be made.
	//! \sa setShadowType()
	//! \return The currently selected shadows' type.
	ShadowType shadowType() const;

	//! Sets the type of shadows to be used with object of this scene. There are several basic types
	//! from which a selection could be made.
	//! \sa shadowType()
	//! \param type The shadows' type.
	void setShadowType(ShadowType type);

	//! Gets the current ambient light's color for the scene.
	//! \sa setAmbientLightColor()
	//! \return The current ambient light's color.
	QColor ambientLightColor() const;

	//! Sets the current ambient light's color for the scene.
	//! \sa ambientLightColor()
	//! \param color The ambient light's color.
	void setAmbientLightColor(const QColor & color);

	//! Retrieves the scene's root node.
	//! \return The scene's root node.
	QOgreNode * rootNode();

	//! Retrieves the scene's immutable root node.
	//! \return The scene's root node.
	const QOgreNode * rootNode() const;

signals:
	//! This signal is emited when the scene is ready to use - objects could be added.
	//! \param The ready scene
	void ready(QOgreScene *);

protected slots:
	//! Prepares the scene. Emits the ready signal.
	void prepare();
};

//! This class represents a generic scene. A generic scene does not include any ways to
//! order/optimize the objects it contains, so it's suitable for small setups, or when the object
//! management is done externally.
class QOGRE_API QOgreGenericScene : public QOgreScene
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreGenericScene)
	QOGRE_OBJECT(QOgreScene)

public:
	//! The constructor. Creates and initializes a generic scene.
	//! \param world The world the scene should be attached to.
	explicit QOgreGenericScene(QOgreWorld * world = QOgreWorld::world());
};

//! This class represents an interior scene. The interior scene collects and manages objects in a
//! fashion that makes it suitable for interiors of houses, caves etc.
class QOGRE_API QOgreInteriorScene : public QOgreScene
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreInteriorScene)
	QOGRE_OBJECT(QOgreScene)

public:
	//! The constructor. Creates and initializes an interior scene.
	//! \param world The world the scene should be attached to.
	explicit QOgreInteriorScene(QOgreWorld * world = QOgreWorld::world());
};

//! This class represents an exterior scene. The exterior scene collects and manages objects in a
//! fashion that makes it suitable for open spaces like meadows, forests etc. There are few distinct
//! types of exterior scenes which are dependent on the distances between objects.
class QOGRE_API QOgreExteriorScene : public QOgreScene
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreExteriorScene)
	QOGRE_OBJECT(QOgreScene)

public:
	//! The scene type is used to differentiate exterior scenes based on the physical distances.
	//! If the distances are small the CloseScene type is appropriate, if they are very large the
	//! VeryFarScene type is suitable, for mid-distance scenes the FarScene is a common choice.
	enum SceneType { CloseScene, FarScene, VeryFarScene };

	//! The constructor. Creates and initializes an exterior scene. Registers the scene name, so it
	//! can be used to retrieve the object. The scene type is dependent on the distances between
	//! objects and is used to optimize performance.
	//! \param world The world the scene should be attached to.
	//! \param type The scene type.
	explicit QOgreExteriorScene(QOgreWorld * world = QOgreWorld::world(), SceneType type = FarScene);
};

#endif // QOGRESCENE_H
