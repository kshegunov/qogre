#ifndef QOGREOBJECT_H
#define QOGREOBJECT_H

#include "qogre.h"

#include <QObject>

class QOgreObjectPrivate;

//! The QOhreObject class is a base for Ogre3D objects.
//! Every class which wants to utilize a signal-oriented destruction could inherit QOgreObject. The
//! class provides means for the PIMPL idiom as well. The private object for a class subclassing
//! QOgreObject shall inherit the QOgreObjectPrivate to ensure the PIMPL idiom works as expected.
class QOGRE_API QOgreObject : public QObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreObject)
	QOGRE_OBJECT(QOgreObject)

protected:
	//! The ogre object constructor.
	//! Initializes the object's d-pointer and parent.
	//! \param d The private object.
	//! \param parent The parent object.
	QOgreObject(QOgreObjectPrivate & d, QObject * parent);

public:
	//! The destructor. If the d-pointer is valid it will try to free it using delete.
	//! \sa shutdown()
	virtual ~QOgreObject();

signals:
	//! The signal is emited when the object is about to be destroyed.
	//! \sa shutdown()
	void aboutToShutdown(QOgreObject *);

protected slots:
	//! Destroys the object and frees the d-pointer. The object will no longer be valid after the
	//! slot is invoked.
	//! \sa aboutToShutdown()
	virtual void shutdown();

protected:
	QOgreObjectPrivate * d_ptr;		//!<  A pointer to the private object (PIMPL idiom).
};

#endif // QOGREOBJECT_H
