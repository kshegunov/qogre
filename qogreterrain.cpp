#include "qogreterrain.h"
#include "qogreterrain_p.h"
#include "qogreterraintile.h"
#include "qogreworld.h"
#include "qogremath.h"
#include "qogrelight.h"

#include <QString>

QOgreTerrain::QOgreTerrain(QOgreScene * scene)
	: QOgreObject(*new QOgreTerrainPrivate(this, scene->world()), scene)
{
	d()->world->setTerrain(this);
}

QOgreScene * QOgreTerrain::scene()
{
	return qobject_cast<QOgreScene *>(parent());
}

const QOgreScene * QOgreTerrain::scene() const
{
	return qobject_cast<QOgreScene *>(parent());
}

QOgreTerrainOptions & QOgreTerrain::options()
{
	return d()->options;
}

const QOgreTerrainOptions & QOgreTerrain::options() const
{
	return d()->options;
}

void QOgreTerrain::setAlignment(QtOgre::PrincipalPlanes plane)
{
	d()->alignment = plane;
}

QtOgre::PrincipalPlanes QOgreTerrain::alignment() const
{
	return d()->alignment;
}

void QOgreTerrain::setTileSize(quint16 size)
{
	Q_ASSERT_X(qIsPowerOfTwo(size), "QOgreTerrain::setTileSize", "Terrain tile size must be an exact power of two");
	d()->tileSize = size;
}

quint16 QOgreTerrain::tileSize() const
{
	return d()->tileSize;
}

void QOgreTerrain::setSize(QSize size)
{
	d()->terrainSize = size;
}

void QOgreTerrain::setSize(qint32 width, qint32 height)
{
	d()->terrainSize = QSize(width, height);
}

QSize QOgreTerrain::size() const
{
	return d()->terrainSize;
}

void QOgreTerrain::setWorldSize(qreal size)
{
	d()->worldSize = size;
}

qreal QOgreTerrain::worldSize() const
{
	return d()->worldSize;
}

void QOgreTerrain::setOrigin(const QVector3D & origin)
{
	d()->origin = origin;
}

QVector3D QOgreTerrain::origin() const
{
	return d()->origin;
}

void QOgreTerrain::setLevelsOfDetail(qint8 lod)
{
	d()->lod = lod;
}

qint8 QOgreTerrain::levelsOfDetail() const
{
	return d()->lod;
}

void QOgreTerrain::setLight(QOgreLight * light)
{
	d()->light = light;
}

QOgreLight * QOgreTerrain::light()
{
	return d()->light;
}

const QOgreLight * QOgreTerrain::light() const
{
	return d()->light;
}

QOgreTerrainTile & QOgreTerrain::tile(QPoint position)
{
	QOGRE_D(QOgreTerrain);
	return *d->tiles[position.x() * d->terrainSize.width() + position.y()];
}

const QOgreTerrainTile & QOgreTerrain::tile(QPoint position) const
{
	QOGRE_D(const QOgreTerrain);
	return *d->tiles[position.x() * d->terrainSize.width() + position.y()];
}

QOgreTerrainTile & QOgreTerrain::tile(qint32 x, qint32 y)
{
	QOGRE_D(QOgreTerrain);
	return *d->tiles[x * d->terrainSize.width() + y];
}

const QOgreTerrainTile & QOgreTerrain::tile(qint32 x, qint32 y) const
{
	QOGRE_D(const QOgreTerrain);
	return *d->tiles[x * d->terrainSize.width() + y];
}

void QOgreTerrain::addLayer(const QOgreTerrainLayer & layer)
{
	d()->layers.append(layer);
}

void QOgreTerrain::setWorld(QOgreWorld * world)
{
	d()->world = world;
}

QOgreWorld * QOgreTerrain::world()
{
	return d()->world;
}

const QOgreWorld * QOgreTerrain::world() const
{
	return d()->world;
}

void QOgreTerrain::setPageSize(QSize size)
{
	d()->pageSize = size;
}

void QOgreTerrain::setPageSize(qint32 x, qint32 y)
{
	d()->pageSize = QSize(x, y);
}

QSize QOgreTerrain::pageSize()
{
	return d()->pageSize;
}

void QOgreTerrain::initialize()
{
	d()->initialize();

/*
	// Init blend maps
	Ogre::Real minHeight0 = 2;
	Ogre::Real fadeDist0 = 5;
	Ogre::Real minHeight1 = 2;
	Ogre::Real fadeDist1 = 1;

	Ogre::Terrain * terrain = d->terrainGroup->getTerrain(0, 0);

	Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);
	Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);

	float* pBlend0 = blendMap0->getBlendPointer();
	float* pBlend1 = blendMap1->getBlendPointer();

	for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); ++y)
	{
	  for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); ++x)
	  {
		Ogre::Real tx, ty;

		blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);
		Ogre::Real height = terrain->getHeightAtTerrainPosition(tx, ty);
		Ogre::Real val = (height - minHeight0) / fadeDist0;
		val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
		*pBlend0++ = val;

		val = (height - minHeight1) / fadeDist1;
		val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
		*pBlend1++ = val;
	  }
	}

	blendMap0->dirty();
	blendMap1->dirty();
	blendMap0->update();
	blendMap1->update();
*/
/*	qDebug() << "Terrains loaded." << endl;
	d->terrainGroup->freeTemporaryResources();
*/
	emit initialized();

}

void QOgreTerrain::update()
{
//	d()->terrainGroup->update();
}

