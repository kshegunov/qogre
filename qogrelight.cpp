#include "qogrelight.h"
#include "qogrelight_p.h"
#include "qogrescene.h"
#include "qogrecast.h"

QOgreLight::QOgreLight(QOgreLightPrivate & d, QOgreScene * parent)
	: QOgreObject(d, parent)
{
}

QOgreLight::~QOgreLight()
{
}

bool QOgreLight::isValid() const
{
	return d()->isValid();
}

QColor QOgreLight::diffuseColor() const
{
	return qogre_cast(d()->light->getDiffuseColour());
}

QColor QOgreLight::specularColor() const
{
	return qogre_cast(d()->light->getSpecularColour());
}

void QOgreLight::setDiffuseColor(const QColor & color)
{
	d()->light->setDiffuseColour(qogre_cast(color));
}

void QOgreLight::setSpecularColor(const QColor & color)
{
	d()->light->setSpecularColour(qogre_cast(color));
}
