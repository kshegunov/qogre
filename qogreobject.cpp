#include "qogreobject.h"
#include "qogreobject_p.h"

QOgreObject::QOgreObject(QOgreObjectPrivate & d, QObject * parent)
	: QObject(parent), d_ptr(&d)
{
	QOgreObject * ooParent = qobject_cast<QOgreObject *>(parent);
	if (ooParent)
		QObject::connect(ooParent, SIGNAL(aboutToShutdown(QOgreObject *)), this, SLOT(shutdown()));
}

QOgreObject::~QOgreObject()
{
	shutdown();
}

void QOgreObject::shutdown()
{
	if (d_ptr)  {
		emit aboutToShutdown(this);

		delete d_ptr;
		d_ptr = NULL;
	}
}
