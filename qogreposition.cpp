#include "qogreposition.h"
#include "qogreposition_p.h"
#include "qogrecast.h"

QOgrePosition::QOgrePosition(QOgrePositionPrivate & d)
	: d_ptr(&d)
{
}

void QOgrePosition::change(const QVector3D & vector)
{
	d()->node->setPosition(qogre_cast(vector));
}

void QOgrePosition::change(qreal x, qreal y, qreal z)
{
	d()->node->setPosition(x, y, z);
}

void QOgrePosition::move(const QVector3D & vector, PositionType position)
{
	d()->node->translate(qogre_cast(vector), qogre_cast(position));
}

void QOgrePosition::move(qreal x, qreal y, qreal z, PositionType position)
{
	d()->node->translate(x, y, z, qogre_cast(position));
}

QOgrePosition::operator QVector3D () const
{
	return qogre_cast(d()->node->getPosition());
}
