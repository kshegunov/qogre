#include "qogrewidget.h"
#include "qogrewidget_p.h"
#include "qogrecast.h"
#include "qogrecamera.h"
#include "qogreengine.h"

#include <QCloseEvent>
#include <QPaintEvent>
#include <QResizeEvent>

#include <QLayout>

QOgreWidget::QOgreWidget(QWidget * parent, Qt::WindowFlags flags)
	: QWidget(parent, flags), d_ptr(new QOgreWidgetPrivate(this))
{
	setAttribute(Qt::WA_PaintOnScreen);
	setAttribute(Qt::WA_DontCreateNativeAncestors);
	setAttribute(Qt::WA_OpaquePaintEvent);
	setAttribute(Qt::WA_NoSystemBackground);
	setAttribute(Qt::WA_NativeWindow);

	QOgreEngine * engine = QOgreEngine::instance();
	QObject::connect(engine, SIGNAL(aboutToShutdown(QOgreObject *)), this, SLOT(shutdown()));
	QObject::connect(engine, SIGNAL(started()), this, SLOT(setup()));
	QObject::connect(this, SIGNAL(ready(QOgreWidget *)), engine, SLOT(loadAllResources()), Qt::QueuedConnection);
}

QOgreWidget::~QOgreWidget()
{
	shutdown();
	delete d_ptr;
}

bool QOgreWidget::isValid() const
{
	return d()->isValid();
}

QOgreCamera * QOgreWidget::camera()
{
	return d()->camera;
}

const QOgreCamera * QOgreWidget::camera() const
{
	return d()->camera;
}

void QOgreWidget::setCamera(QOgreCamera * camera)
{
	QOGRE_D(QOgreWidget);
	if (camera == d->camera)
		return;

	if (d->window && d->viewport)
		d->window->removeViewport(d->viewport->getZOrder());

	d->camera = camera;

	Ogre::Camera * ogreCamera = qogre_cast(camera);
	d->viewport = d->window->addViewport(ogreCamera);
	ogreCamera->setAspectRatio(static_cast<qreal>(width()) / height());

	emit cameraChanged(camera);
}

void QOgreWidget::setBackgroundColor(const QColor & color)
{
	d()->viewport->setBackgroundColour(qogre_cast(color));
}

QColor QOgreWidget::backgroundColor() const
{
	return qogre_cast(d()->viewport->getBackgroundColour());
}

void QOgreWidget::enableAutoUpdate(bool enable)
{
	d()->autoUpdate = enable;
}

void QOgreWidget::disableAutoUpdate(bool disable)
{
	d()->autoUpdate = !disable;
}

bool QOgreWidget::isAutoUpdated() const
{
	return d()->autoUpdate;
}

QPaintEngine * QOgreWidget::paintEngine() const
{
	return NULL;
}

void QOgreWidget::setup()
{
	d()->initialize();

	emit ready(this);
}

void QOgreWidget::shutdown()
{
	emit aboutToShutdown();

	// TODO: Wait for ogre to finish rendering (if any is pending).

	d()->shutdown();
}

void QOgreWidget::closeEvent(QCloseEvent * event)
{
	QWidget::closeEvent(event);

	event->accept();
	shutdown();
}

void QOgreWidget::paintEvent(QPaintEvent * event)
{
	QWidget::paintEvent(event);

	if (d()->autoUpdate)
		qogre_cast(QOgreEngine::instance())->renderOneFrame();;

	event->accept();
}

void QOgreWidget::resizeEvent(QResizeEvent * event)
{
	QWidget::resizeEvent(event);

	QOGRE_D(QOgreWidget);
	if (!event->spontaneous() && !d->isValid())
		return;

	d->resize(event->size());
	QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);

	event->accept();
}
