#include "qogremesh.h"
#include "qogrewidget.h"
#include "qogrecast.h"
#include "qogrescene.h"
#include "qogremesh_p.h"
#include "qogreposition.h"
#include "qogreenvelope.h"

#include <OgreAxisAlignedBox.h>

QOgreMesh::QOgreMesh(const QString & file, QOgreWorld * world)
	: QOgreNode(*new QOgreMeshPrivate(file, world->scene(), this), world->scene())
{
}

QOgreMesh::QOgreMesh(const QString & file, QOgreScene * scene)
	: QOgreNode(*new QOgreMeshPrivate(file, scene, this), scene)
{
}

QOgreMesh::QOgreMesh(const QString & file, QOgreNode * parent)
	: QOgreNode(*new QOgreMeshPrivate(file, parent, this), parent)
{
}

QOgreMesh::~QOgreMesh()
{
}

bool QOgreMesh::isValid() const
{
	return d()->isValid();
}

QOgreEnvelope QOgreMesh::envelope() const
{
	const Ogre::AxisAlignedBox & box = d()->entity->getBoundingBox();
	Ogre::Vector3 size = box.getSize();

	return QOgreEnvelope(qogre_cast(box.getCenter()), QOgreSize(size.x, size.y, size.z));
}
