#ifndef QOGRETERRAINLAYER_H
#define QOGRETERRAINLAYER_H

#include "qogre.h"

#include <QSharedDataPointer>
#include <QStringList>

class QOgreTerrainLayerPrivate;
class QOGRE_API QOgreTerrainLayer
{
public:
	QOgreTerrainLayer(qreal worldSize = 1, const QStringList & textures = QStringList());
	QOgreTerrainLayer(const QOgreTerrainLayer &);
	~QOgreTerrainLayer();
	QOgreTerrainLayer & operator = (const QOgreTerrainLayer &);

	qreal worldSize() const;
	void setWorldSize(qreal);

	QStringList textures() const;
	void setTextures(const QStringList &);
	void addTexture(const QString &);
	void removeTexture(const QString &);

	QOgreTerrainLayer & operator << (const QString &);

private:
	QSharedDataPointer<QOgreTerrainLayerPrivate> d;
};

// -------------------------------------------------------------------------------------------------------- //

inline QOgreTerrainLayer & QOgreTerrainLayer::operator << (const QString & texture)
{
	addTexture(texture);
	return *this;
}

#endif // QOGRETERRAINLAYER_H
