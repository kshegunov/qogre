#ifndef QOGRESIZE_H
#define QOGRESIZE_H

#include "qogre.h"

#include <QTypeInfo>

//! The size class represents an extent in three dimensions.
//! Every visible or invisible object could have a size, and this class provides the common API for
//! retrieving and modifying it.
class QOGRE_API QOgreSize
{
public:
	//! The default constructor.
	//! Creates an empty size.
	QOgreSize();

	//! The size constructor.
	//! Creates a size object from its dimensions.
	//! \param width The width.
	//! \param height The height.
	//! \param depth The depth.
	QOgreSize(qreal width, qreal height, qreal depth);

	//! Checks if the size is valid.
	//! If one of the dimensions is equal to zero, then it's considered invalid.
	//! \return Whether the size is valid.
	bool isValid() const;

	//! Gets the width.
	//! \sa setWidth()
	//! \return The width.
	qreal width() const;

	//! Sets the width.
	//! \sa width()
	//! \param width The width.
	void setWidth(qreal width);

	//! Gets the height.
	//! \sa setHeight()
	//! \return The height.
	qreal height() const;

	//! Sets the height.
	//! \sa height()
	//! \param height The height.
	void setHeight(qreal height);

	//! Gets the depth.
	//! \sa setDepth()
	//! \return The depth.
	qreal depth() const;

	//! Sets the depth.
	//! \sa depth()
	//! \param depth The depth.
	void setDepth(qreal depth);

	//! Gets the maximum extent in any of the three directions.
	//! \sa width(), height() and depth()
	//! \return The maximum extent.
	qreal max() const;

private:
	qreal w;	//!< The width.
	qreal h;	//!< The height.
	qreal d;	//!< The depth.
};
Q_DECLARE_TYPEINFO(QOgreSize, Q_MOVABLE_TYPE);	// Tell Qt it may use memcpy/memmove for this object


// --------------------------------------------------------------------------------------- //
// --- QOgreSize implementation ---------------------------------------------------------- //
// --------------------------------------------------------------------------------------- //

inline QOgreSize::QOgreSize()
	: w(0), h(0), d(0)
{
}

inline QOgreSize::QOgreSize(qreal width, qreal height, qreal depth)
	: w(width), h(height), d(depth)
{
}

inline bool QOgreSize::isValid() const
{
	return !qFuzzyIsNull(w) && !qFuzzyIsNull(h) && !qFuzzyIsNull(d);
}

inline qreal QOgreSize::width() const
{
	return w;
}

inline void QOgreSize::setWidth(qreal width)
{
	w = width;
}

inline qreal QOgreSize::height() const
{
	return h;
}

inline void QOgreSize::setHeight(qreal height)
{
	h = height;
}

inline qreal QOgreSize::depth() const
{
	return d;
}

inline void QOgreSize::setDepth(qreal depth)
{
	d = depth;
}

inline qreal QOgreSize::max() const
{
	return qMax(w, qMax(h, d));
}

#endif // QOGRESIZE_H
