#include "qogreorientation.h"
#include "qogreorientation_p.h"
#include "qogrecast.h"

#include <QVector3D>
#include <QtMath>

QOgreOrientation::QOgreOrientation(QOgreOrientationPrivate & d)
	: d_ptr(&d)
{
}

QVector3D QOgreOrientation::axis() const
{
	Ogre::Vector3 axis;
	Ogre::Degree angle;

	d()->node->getOrientation().ToAngleAxis(angle, axis);
	return qogre_cast(axis);
}

void QOgreOrientation::change(const QQuaternion & quaternion)
{
	d()->node->setOrientation(qogre_cast(quaternion.normalized()));
}

void QOgreOrientation::change(const QVector3D & axis, qreal angle)
{
	d()->node->setOrientation(angle, axis.x(), axis.y(), axis.z());
}

void QOgreOrientation::change(qreal x, qreal y, qreal z, qreal angle)
{
	d()->node->setOrientation(angle, x, y, z);
}

void QOgreOrientation::rotate(const QQuaternion & rotation)
{
	d()->node->rotate(qogre_cast(rotation));
}

void QOgreOrientation::rotate(qreal angle, const QVector3D & axis)
{
	d()->node->rotate(qogre_cast(axis), Ogre::Degree(angle));
}

void QOgreOrientation::rotate(qreal angle, QtOgre::PrincipalAxes axis)
{
	QOGRE_D(QOgreOrientation);
	switch (axis)
	{
	case QtOgre::XAxis:
		d->node->rotate(Ogre::Vector3::UNIT_X, Ogre::Degree(angle));
		break;
	case QtOgre::YAxis:
		d->node->rotate(Ogre::Vector3::UNIT_Y, Ogre::Degree(angle));
		break;
	case QtOgre::ZAxis:
		d->node->rotate(Ogre::Vector3::UNIT_Z, Ogre::Degree(angle));
		break;
	}
}

void QOgreOrientation::rotate(qreal angle, QtOgre::PrincipalPlanes plane)
{
	QOGRE_D(QOgreOrientation);

	// Get the rotation axis from the plane normal and the node's position
	Ogre::Vector3 axis, position = d->node->getPosition();
	switch (plane)
	{
	case QtOgre::XYPlane:
		axis = position.crossProduct(Ogre::Vector3::UNIT_Z);
		break;
	case QtOgre::XZPlane:
		axis = position.crossProduct(Ogre::Vector3::UNIT_Y);
		break;
	case QtOgre::YZPlane:
		axis = position.crossProduct(Ogre::Vector3::UNIT_X);
		break;
	default:
		return;
	}
	axis.normalise();

	d->node->rotate(axis, Ogre::Degree(angle));
}

void QOgreOrientation::rotate(qreal angle, const QVector3D & vector, QtOgre::PrincipalPlanes plane)
{
	Ogre::Vector3 axis, position = qogre_cast(vector);
	switch (plane)
	{
	case QtOgre::XYPlane:
		axis = position.crossProduct(Ogre::Vector3::UNIT_Z);
		break;
	case QtOgre::XZPlane:
		axis = position.crossProduct(Ogre::Vector3::UNIT_Y);
		break;
	case QtOgre::YZPlane:
		axis = position.crossProduct(Ogre::Vector3::UNIT_X);
		break;
	default:
		return;
	}
	axis.normalise();

	d()->node->rotate(axis, Ogre::Degree(angle));
}

void QOgreOrientation::reset()
{
	d()->node->resetOrientation();
}

QOgreOrientation::operator QQuaternion () const
{
	return qogre_cast(d()->node->getOrientation());
}

void QOgreOrientation::yaw(qreal angle)
{
	d()->node->yaw(Ogre::Degree(angle), Ogre::Node::TS_PARENT);
}

void QOgreOrientation::pitch(qreal angle)
{
	d()->node->pitch(Ogre::Degree(angle), Ogre::Node::TS_PARENT);
}

void QOgreOrientation::roll(qreal angle)
{
	d()->node->roll(Ogre::Degree(angle), Ogre::Node::TS_PARENT);
}
