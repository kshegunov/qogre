#ifndef QOGRECAMERA_H
#define QOGRECAMERA_H

#include "qogre.h"
#include "qogrenode.h"

class QVector3D;
class QOgreCameraPrivate;

//! The camera class manages a camera for a scene.
//! The camera is created automatically by the ogre widget (containing the scene) when it's needed
//! and provides the interface to manipulate its position and/or orientation.
class QOGRE_API QOgreCamera : public QOgreNode
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreCamera)
	QOGRE_OBJECT(QOgreCamera)

public:
	//! The camera constructor.
	//! Creates and initializes the camera.
	//! \param scene The scene the camera relates to
	explicit QOgreCamera(QOgreScene * scene);

	//! Check if the camera is properly initialized. If the camera is valid it's ready to be used.
	//! \return Whether the camera is valid.
	bool isValid() const;

	//! Gets the camera's scene manager.
	//! \return The camera's scene.
	QOgreScene * scene();

	//! Gets the camera's scene manager.
	//! \return The camera's immutable scene.
	const QOgreScene * scene() const;

	//! Sets the camera to look at a node and anchors it to it.
	//! It causes the camera to reorient itself so the provided point is in the center of its visual
	//! field. The new position and orientation could be fine tuned afterwards by calling the
	//! appropriate functions.
	//! \sa position() and orientation()
	//! \param position The node the camera should be attached to.
	void setAnchor(QOgreNode *);

	//! Gets the camera's anchor.
	//! \sa setAnchor()
	//! \return The camera's anchor.
	QOgreNode * anchor();

	//! Gets the camera's immutable anchor.
	//! \sa setAnchor()
	//! \return The camera's anchor.
	const QOgreNode * anchor() const;

	//! Detaches the camera from its anchor.
	//! \sa setAnchor(), anchor()
	//! \return The camera's anchor.
	QOgreNode * detachAnchor();

	//! Sets the camera near-clip distance.
	//! \param distance The camera near-clip distance.
	void setNearClipDistance(qreal distance);

	//! Gets the camera near-clip distance.
	//! \return The camera near-clip distance.
	qreal nearClipDistance() const;

	//! Sets the camera far-clip distance.
	//! \param distance The camera far-clip distance.
	void setFarClipDistance(qreal distance);

	//! Gets the camera far-clip distance.
	//! \return The camera far-clip distance.
	qreal farClipDistance() const;

	//! Rotates the camera around a global axis that passes through the anchor point.
	//! \param angle The angle of rotation.
	//! \param axis The axis of rotation.
	void rotate(qreal angle, QtOgre::PrincipalAxes axis);

	//! Rotates the camera around a global axis that passes through the anchor point is part
	//! of the specified plane and is perpendicular to the camera's orientation.
	//! \param angle The angle of rotation
	//! \param plane The principal plane that contains the axis of rotation.
	void rotate(qreal angle, QtOgre::PrincipalPlanes plane);

	//! Moves the camera with a vector offset
	//! \param offset The direction and magnitude the camera should be moved.
	void move(const QVector3D & offset);

	//! Resets the camera orientation.
	void reset();

};

#endif // QOGRECAMERA_H
