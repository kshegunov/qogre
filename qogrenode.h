#ifndef QOGRENODE_H
#define QOGRENODE_H

#include "qogreobject.h"
#include "qogreworld.h"

class QOgrePosition;
class QOgreOrientation;
class QOgreScene;

class QOgreNodePrivate;
class QOGRE_API QOgreNode : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreNode)
	QOGRE_OBJECT(QOgreNode)

public:
	//! Constructor. Creates a node in the scene and initializes it.
	//! \param world The world in which a node should be created.
	explicit QOgreNode(QOgreWorld * world = QOgreWorld::world());

	//! Constructor. Creates a node in the scene and initializes it.
	//! \param scene The scene in which a node should be created.
	explicit QOgreNode(QOgreScene * scene);

	//! Constructor. Creates a node in the scene with a given parent node and initializes it.
	//! \param parent The parent node to which the newly created one should be attached.
	explicit QOgreNode(QOgreNode * parent);

	//! Constructor. Used in derived classes to initialize the node with its private object.
	//! \param d The Node's private object (or derived class' private object).
	//! \param parent The node's parent QObject.
	QOgreNode(QOgreNodePrivate & d, QOgreObject * parent);

	//! Destructor. Destroys the node and releases the resources associated with it.
	virtual ~QOgreNode();

	//! Retrieves the node's position object.
	//! Returns a reference to the node's position and allows manipulating it.
	//! \return The node's position.
	QOgrePosition & position();

	//! Retrieves the node's position object.
	//! Returns a reference to the node's position.
	//! \return The node's immutable position.
	const QOgrePosition & position() const;

	//! Retrieves the node's orientation object.
	//! Returns a reference to the node's orientation and allows manipulating it.
	//! \return The node's orientation.
	QOgreOrientation & orientation();

	//! Retrieves the node's orientation object.
	//! Returns a reference to the node's orientation.
	//! \return The node's immutable orientation.
	const QOgreOrientation & orientation() const;

	//! Retrieves the current node's parent.
	//! \return The current node's parent.
	QOgreNode * parent();

	//! Retrieves the current node's immutable parent.
	//! \return The current node's immutable parent.
	const QOgreNode * parent() const;

	//! Retrieves the node's scene.
	//! \return The node's scene.
	QOgreScene * scene();

	//! Retrieves the node's scene.
	//! \return The node's scene.
	const QOgreScene * scene() const;
};

#endif // QOGRENODE_H
