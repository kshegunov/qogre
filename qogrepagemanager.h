#ifndef QOGREPAGEMANAGER_H
#define QOGREPAGEMANAGER_H

#include "qogreobject.h"

class QOgreEngine;
class QOgreCamera;

class QOGRE_API QOgrePageManager : public QOgreObject
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgrePageManager)
	QOGRE_OBJECT(QOgrePageManager)

public:
	QOgrePageManager(QOgreEngine *);

public slots:
	void addCamera(QOgreObject *);
	void removeCamera(QOgreObject *);
};

#endif // QOGREPAGEMANAGER_H
