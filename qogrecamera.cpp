#include "qogrecamera.h"
#include "qogrecamera_p.h"
#include "qogrecast.h"
#include "qogrescene.h"

#include <QVector3D>

QOgreCamera::QOgreCamera(QOgreScene * scene)
	: QOgreNode(*new QOgreCameraPrivate(scene, this), scene)
{
	QOgreEngine::instance()->pageManager()->addCamera(this);
}

bool QOgreCamera::isValid() const
{
	return d()->isValid();
}

QOgreScene * QOgreCamera::scene()
{
	return dynamic_cast<QOgreScene *>(parent());
}

const QOgreScene * QOgreCamera::scene() const
{
	return dynamic_cast<const QOgreScene *>(parent());
}

void QOgreCamera::setAnchor(QOgreNode * node)
{
	d()->attachAnchor(node);
}

QOgreNode * QOgreCamera::anchor()
{
	return d()->anchor;
}

const QOgreNode * QOgreCamera::anchor() const
{
	return d()->anchor;
}

QOgreNode * QOgreCamera::detachAnchor()
{
	return d()->detachAnchor();
}

void QOgreCamera::setNearClipDistance(qreal distance)
{
	d()->camera->setNearClipDistance(distance);
}

qreal QOgreCamera::nearClipDistance() const
{
	return d()->camera->getNearClipDistance();
}

void QOgreCamera::setFarClipDistance(qreal distance)
{
	d()->camera->setFarClipDistance(distance);
}

qreal QOgreCamera::farClipDistance() const
{
	return d()->camera->getFarClipDistance();
}

void QOgreCamera::rotate(qreal angle, QtOgre::PrincipalAxes axis)
{
	QOGRE_D(QOgreCamera);
	if (!d->anchor)
		return;

	d->anchor->orientation().rotate(angle, axis);
}

void QOgreCamera::rotate(qreal angle, QtOgre::PrincipalPlanes plane)
{
	QOGRE_D(QOgreCamera);
	if (!d->anchor)
		return;

	d->anchor->orientation().rotate(angle, position(), plane);
}

void QOgreCamera::move(const QVector3D & offset)
{
	QOGRE_D(QOgreCamera);

	if (d->anchor)
		d->anchor->position().move(offset);
	else
		d->node->translate(qogre_cast(offset));
}

void QOgreCamera::reset()
{
	QOGRE_D(QOgreCamera);
	if (!d->anchor)
		return;

	d->anchor->orientation().reset();
}
