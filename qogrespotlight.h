#ifndef QOGRESPOTLIGHT_H
#define QOGRESPOTLIGHT_H

#include "qogrelight.h"

class QOgrePosition;
class QOgreOrientation;
class QOgreSpotLightPrivate;

//! This class represents a spot light.
//! The spot lights emits a cone of light in a single direction. If the renderer will allow it the
//! light cone's intensity may change in the radial direction thus providing a more realistic view.
//! Usefull for artifical indoors spot lights, holes in caves, flashlights etc.
class QOGRE_API QOgreSpotLight : public QOgreLight
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreSpotLight)
	QOGRE_OBJECT(QOgreSpotLight)

public:
	//! The spot light constructor.
	//! Creates and initializes the light.
	//! \sa QOgreLight::QOgreLight()
	//! \param scene The scene to which the light is attached.
	explicit QOgreSpotLight(QOgreScene * scene);

	//! Retrieves the light's position object.
	//! Returns a reference to the light's position and allows manipulating it.
	//! \return The light's position.
	QOgrePosition & position();

	//! Retrieves the light's position object.
	//! Returns a reference to the light's position.
	//! \return The light's immutable position.
	const QOgrePosition & position() const;

	//! Retrieves the light's orientation object.
	//! Returns a reference to the light's orientation and allows manipulating it.
	//! \return The light's orientation.
	QOgreOrientation & orientation();

	//! Retrieves the light's orientation object.
	//! Returns a reference to the light's orientation.
	//! \return The light's immutable orientation.
	const QOgreOrientation & orientation() const;

	//! Gets the falloff between the spotlight's inner and outer cones.
	//! \sa setFalloff()
	//! \return The falloff between the inner and outer cones.
	qreal falloff() const;

	//! Sets the falloff between the spotlight's inner and outer cones.
	//! \sa falloff()
	//! \param falloff The falloff between the inner and outer cones.
	void setFalloff(qreal falloff);

	//! Gets the the angle of the spotlight's innner cone.
	//! \sa setInnerAngle()
	//! \return The inner cone's angle (in degrees).
	qreal innerAngle() const;

	//! Sets the the angle of the spotlight's innner cone.
	//! \sa innerAngle()
	//! \param angle The inner cone's angle (in degrees).
	void setInnerAngle(qreal angle);

	//! Gets the the angle of the spotlight's outer cone.
	//! \sa setOuterAngle()
	//! \return The outer cone's angle (in degrees).
	qreal outerAngle() const;

	//! Sets the the angle of the spotlight's outer cone.
	//! \sa outerAngle()
	//! \param angle The outer cone's angle (in degrees).
	void setOuterAngle(qreal angle);

	//! Gets the spotlight's near-clip distance.
	//! Relevant if light clipping is supported, allowing to render spots as if they start from
	//! further down than the frustrum.
	//! \sa setNearClipDistance()
	//! \return The near-clip distance.
	qreal nearClipDistance() const;

	//! Sets the spotlight's near-clip distance.
	//! Relevant if light clipping is supported, allowing to render spots as if they start from
	//! further down than the frustrum.
	//! \sa nearClipDistance()
	//! \param distance The near-clip distance.
	void setNearClipDistance(qreal distance);
};

#endif // QOGRESPOTLIGHT_H
