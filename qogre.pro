#-------------------------------------------------
#
# Project created by QtCreator 2013-07-14T16:33:45
#
#-------------------------------------------------

MAKEFILE = qogre.make

TARGET = qogre
TEMPLATE = lib

QT += core gui widgets
CONFIG += create_prl

INCLUDEPATH += ./private

DEFINES += QOGRE_LIBRARY

OGRE_PATH = C:/Programming/ogresdk
BOOST_PATH = C:/Boost

#Platform-specific settings
win32  {
	DESTDIR = $$OUT_PWD		# Fix for (windows') shadow building

		INCLUDEPATH +=  $$OGRE_PATH/include/OGRE $$OGRE_PATH/include/OGRE/Paging $$BOOST_PATH/include/boost-1_59
	CONFIG(debug, debug|release)  {
				LIBS += -L$$OGRE_PATH/lib/debug -L$$OGRE_PATH/lib/debug/opt -L$$BOOST_PATH/lib -lOgreMain_d -lOgreOverlay_d -lOgrePaging_d -lOgreProperty_d -lOgreRTShaderSystem_d -lOgreTerrain_d -lOgreVolume_d  -L$$OGRE_PATH/lib/OGRE  -lRenderSystem_GL_d -lPlugin_CgProgramManager_d -lRenderSystem_Direct3D9_d -lRenderSystem_Direct3D11_d
	}
	CONFIG(release, debug|release)  {
		LIBS += -L$$OGRE_PATH/lib/release -L$$OGRE_PATH/boost/lib -lOgreMain
	}
}
unix  {
	QT += x11extras
	QMAKE_CXXFLAGS += -fvisibility=hidden

	INCLUDEPATH += $$OGRE_PATH/include/OGRE

	# --- OGRE static linkage! --------------------------------------------- #
	QMAKE_LFLAGS += -Wl,-whole-archive -L$$OGRE_PATH/lib -lOgreMainStatic -lOgreOverlayStatic -lOgreMeshLodGeneratorStatic -lOgrePagingStatic -lOgrePropertyStatic -lOgreRTShaderSystemStatic -lOgreTerrainStatic -lOgreVolumeStatic  -L$$OGRE_PATH/lib/OGRE  -lRenderSystem_GLStatic -lPlugin_CgProgramManagerStatic -Wl,-no-whole-archive
	LIBS += -lboost_thread -lboost_system -lXt -lXaw -lCg -lGL -lGLU -lXrandr -lfreeimage

	# Using static plugins, so configure accordingly
	DEFINES += QOGRE_STATIC_GL QOGRE_STATIC_CG QOGRE_STATIC_CODECS
	INCLUDEPATH += $$OGRE_PATH/include/OGRE/RenderSystems/GL $$OGRE_PATH/include/OGRE/Plugins/CgProgramManager

	# Install path
	target.path = /usr/lib
	INSTALLS += target
}

SOURCES += \
	qogrewidget.cpp \
	private/qogrewidget_p.cpp \
	qogrescene.cpp \
	qogrecamera.cpp \
	private/qogrescene_p.cpp \
	private/qogrecamera_p.cpp \
	private/qrcarchive.cpp \
	private/qogrelog.cpp \
	qogrerenderer.cpp \
	private/qogrerenderer_p.cpp \
	qogremesh.cpp \
	private/qogremesh_p.cpp \
	qogrelight.cpp \
	private/qogrelight_p.cpp \
	qogreobject.cpp \
	private/qogrecast.cpp \
	private/qogreobject_p.cpp \
	qogreposition.cpp \
	private/qogreposition_p.cpp \
	qogreexception.cpp \
	qogreorientation.cpp \
	private/qogreorientation_p.cpp \
	qogrepointlight.cpp \
	private/qogrepointlight_p.cpp \
	private/qogrespotlight_p.cpp \
	private/qogredirectionallight_p.cpp \
	qogrespotlight.cpp \
	qogredirectionallight.cpp \
	qogreenvelope.cpp \
	qogresize.cpp \
	qogreterrain.cpp \
	private/qogreterrain_p.cpp \
	qogreterraintile.cpp \
	private/qogreterraintile_p.cpp \
	qogreterrainlayer.cpp \
	qogrenode.cpp \
	private/qogrenode_p.cpp \
	qogreengine.cpp \
	private/qogreengine_p.cpp \
	qogre.cpp \
	qogreterrainoptions.cpp \
	private/qogreterrainoptions_p.cpp \
	qogreworld.cpp \
	private/qogreworld_p.cpp \
	qogrepagemanager.cpp \
	private/qogrepagemanager_p.cpp

HEADERS += \
	qogrewidget.h \
	qogrescene.h \
	qogrerenderer.h \
	qogremesh.h \
	qogrecamera.h \
	qogre.h \
	private/qrcarchive.h \
	private/qogrewidget_p.h \
	private/qogrescene_p.h \
	private/qogrerenderer_p.h \
	private/qogremesh_p.h \
	private/qogrelog.h \
	private/qogrecamera_p.h \
	QOgre \
	QtOgre/QOgreWidget \
	QtOgre/QOgreScene \
	QtOgre/QOgreRenderer \
	QtOgre/QOgreMesh \
	QtOgre/QOgreCamera \
	qogrelight.h \
	private/qogrelight_p.h \
	qogreobject.h \
	private/qogrecast.h \
	private/qogreobject_p.h \
	QtOgre/QOgreLight \
	qogreposition.h \
	private/qogreposition_p.h \
	QtOgre/QOgrePosition \
	qogreexception.h \
	QtOgre/QOgreException \
	qogreorientation.h \
	QtOgre/QOgreOrientation \
	private/qogreorientation_p.h \
	qogrepointlight.h \
	private/qogrepointlight_p.h \
	private/qogrespotlight_p.h \
	private/qogredirectionallight_p.h \
	qogrespotlight.h \
	qogredirectionallight.h \
	QtOgre/QOgreDirectionalLight \
	QtOgre/QOgrePointLight \
	QtOgre/QOgreSpotLight \
	qogreenvelope.h \
	qogresize.h \
	QtOgre/QOgreSize \
	QtOgre/QOgreEnvelope \
	qogreterrain.h \
	private/qogreterrain_p.h \
	qogreterraintile.h \
	QtOgre/QOgreTerrain \
	private/qogreterraintile_p.h \
	qogreterrainlayer.h \
	qogrenode.h \
	private/qogrenode_p.h \
	QtOgre/QOgreNode \
	qogreengine.h \
	QtOgre/QOgreEngine \
	private/qogreengine_p.h \
	qogreterrainoptions.h \
	private/qogreterrainoptions_p.h \
	private/qogremath.h \
	qogreworld.h \
	private/qogreworld_p.h \
	qogrepagemanager.h \
	private/qogrepagemanager_p.h \
    QtOgre/QOgrePageManager



