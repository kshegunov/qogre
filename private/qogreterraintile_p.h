#ifndef QOGRETERRAINTILE_P_H
#define QOGRETERRAINTILE_P_H

#include "qogreobject_p.h"

#include <QPoint>

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

class QOgreTerrain;

class QOgreTerrainTile;
class QOGRE_LOCAL QOgreTerrainTilePrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreTerrainTile)

	friend class QOgreTerrain;

	QOgreTerrainTilePrivate(QOgreTerrainTile *, QOgreTerrain *);
	virtual ~QOgreTerrainTilePrivate();

	QOgreTerrain * terrain;
	Ogre::Terrain * tile;
	QPoint position;
};

#endif // QOGRETERRAINTILE_P_H
