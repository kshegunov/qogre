#ifndef QOGREORIENTATION_P_H
#define QOGREORIENTATION_P_H

#include "qogre.h"

#include <OgreNode.h>

class QOgreOrientation;
class QOGRE_LOCAL QOgreOrientationPrivate
{
	friend class QOgreOrientation;

public:
	explicit QOgreOrientationPrivate(Ogre::Node * = NULL);

	void setNode(Ogre::Node *);

private:
	Ogre::Node * node;
};

#endif // QOGREORIENTATION_P_H
