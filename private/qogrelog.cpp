#include "qogrelog.h"

#include <QTextStream>
#include <QDateTime>

void QOgreLog::warning(const QString & message)
{
	instance().cerr << "QOgre[W] @ " + QDateTime::currentDateTime().toString(Qt::ISODate) + " > " + message << endl;
}

void QOgreLog::error(const QString & message)
{
	instance().cerr << "QOgre[E] @ " + QDateTime::currentDateTime().toString(Qt::ISODate) + " > " + message << endl;
}

void QOgreLog::notice(const QString & message)
{
	instance().cerr << "QOgre[N] @ " + QDateTime::currentDateTime().toString(Qt::ISODate) + " > " + message << endl;
}

QOgreLog::QOgreLog()
	: cerr(stderr)
{
	cerr << " ------------------------------------------------------------------- " << endl
		 << QDateTime::currentDateTime().toString(Qt::ISODate) + " | Error log started." << endl
		 << " ------------------------------------------------------------------- " << endl;
}

QOgreLog::~QOgreLog()
{
	cerr << " ------------------------------------------------------------------- " << endl
		 << QDateTime::currentDateTime().toString(Qt::ISODate) + " | Error log finished." << endl
		 << " ------------------------------------------------------------------- " << endl;
}

QOgreLog & QOgreLog::instance()
{
	static QOgreLog object;
	return object;
}
