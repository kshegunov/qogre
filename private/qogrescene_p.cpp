#include "qogrescene_p.h"
#include "qogrescene.h"
#include "qogreengine.h"
#include "qogrecast.h"

#include <OgreRoot.h>

QOgreScenePrivate::QOgreScenePrivate(Ogre::SceneTypeMask typeMask, QOgreScene * q)
	: QOgreObjectPrivate(q), scene(NULL), type(typeMask)
{
	scene = qogre_cast(QOgreEngine::instance())->createSceneManager(type);
}

QOgreScenePrivate::~QOgreScenePrivate()
{
	if (scene)
		qogre_cast(QOgreEngine::instance())->destroySceneManager(scene);		// Destroy the scene
}
