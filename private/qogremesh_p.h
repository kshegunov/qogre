#ifndef QOGREMESH_P_H
#define QOGREMESH_P_H

#include "qogrenode_p.h"

#include <OgreEntity.h>

class QOgreScene;
class QOgreNode;

class QOgreMesh;
class QOGRE_LOCAL QOgreMeshPrivate : public QOgreNodePrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreMesh)

	QOgreMeshPrivate(const QString &, QOgreScene * parent, QOgreMesh *);
	QOgreMeshPrivate(const QString &, QOgreNode * parent, QOgreMesh *);
	virtual ~QOgreMeshPrivate();

	void create(Ogre::SceneManager *, const QString &);

	bool isValid() const;

	Ogre::Entity * entity;
};

inline bool QOgreMeshPrivate::isValid() const
{
	return entity && node;
}

#endif // QOGREMESH_P_H
