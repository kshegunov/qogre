#include "qogrerenderer_p.h"
#include "qogreengine.h"
#include "qogrelog.h"
#include "qogrecast.h"


QHash<QString, QOgreRenderer *> QOgreRendererPrivate::renderers;
QOgreRenderer * QOgreRendererPrivate::currentRenderer = NULL;

QOgreRendererPrivate::QOgreRendererPrivate(const QString & rendererName, QOgreRenderer * q)
	: QOgreObjectPrivate(q), name(rendererName), renderer(NULL), options(0), changed(false), selected(false)
{
	renderers.insert(name, q);
}

QOgreRendererPrivate::~QOgreRendererPrivate()
{
	renderers.remove(name);
}

void QOgreRendererPrivate::flushOptions()
{
	RendererOptionList values = qogre_cast(options);
	foreach (RendererOption option, values)
		renderer->setConfigOption(option.first.toStdString(), option.second.toStdString());
}

void QOgreRendererPrivate::loadRenderers()
{
	// Initialize the renderers list
	using Ogre::RenderSystemList;
	typedef RenderSystemList::const_iterator RenderSystemIterator;

	const RenderSystemList & renderSystems = Ogre::Root::getSingleton().getAvailableRenderers();
	for (RenderSystemIterator i = renderSystems.begin(), end = renderSystems.end(); i != end; i++)  {
		// Initialize the renderer
		QString name = QString::fromStdString((*i)->getName());
		QOgreRenderer * renderer = new QOgreRenderer(name);
		renderer->d()->renderer = *i;
	}

	if (renderers.size() == 0)
		QOgreLog::warning("No render systems were made available through the loaded plugins.");
}


void QOgreRendererPrivate::select()
{
	QOGRE_Q(QOgreRenderer);

	flushOptions();

	Ogre::Root & root = Ogre::Root::getSingleton();
	if (currentRenderer == q && changed)
		renderer->reinitialise();
	else
		root.setRenderSystem(renderer);

	changed = false;
	currentRenderer = q;
}
