#include "qogreterraintile_p.h"
#include "qogreterraintile.h"

QOgreTerrainTilePrivate::QOgreTerrainTilePrivate(QOgreTerrainTile * q, QOgreTerrain * ogreTerrain)
	: QOgreObjectPrivate(q), terrain(ogreTerrain), tile(NULL)
{
}

QOgreTerrainTilePrivate::~QOgreTerrainTilePrivate()
{
}


/*
void QOgreTerrainTilePrivate::initBlendMaps()
{

	// Init blend maps
	Ogre::Real minHeight0 = 70;
	Ogre::Real fadeDist0 = 40;
	Ogre::Real minHeight1 = 70;
	Ogre::Real fadeDist1 = 15;

	Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);
	Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);

	float* pBlend0 = blendMap0->getBlendPointer();
	float* pBlend1 = blendMap1->getBlendPointer();

	for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); y++)
	{
	  for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); x++)
	  {
		Ogre::Real tx, ty;

		blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);
		Ogre::Real height = terrain->getHeightAtTerrainPosition(tx, ty);
		Ogre::Real val = (height - minHeight0) / fadeDist0;
		val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
		*pBlend0++ = val;

		val = (height - minHeight1) / fadeDist1;
		val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
		*pBlend1++ = val;
	  }
	}

	blendMap0->dirty();
	blendMap1->dirty();
	blendMap0->update();
	blendMap1->update();
}
*/
