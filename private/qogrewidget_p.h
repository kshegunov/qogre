#ifndef QOGREWIDGET_P_H
#define QOGREWIDGET_P_H

#include "qogre.h"
#include "qogrewidget.h"
#include "qogrecamera.h"
#include "qogrecamera_p.h"

#include <QPointer>
#include <QTimer>
#include <QList>

#include <OgreRenderWindow.h>
#include <OgreViewport.h>
#include <OgreRenderTargetListener.h>

class QWidget;
class QOgreCamera;
class QOgreWidget;

class QOGRE_LOCAL QOgreWidgetPrivate
{
	class QOGRE_LOCAL Listener : public Ogre::RenderTargetListener
	{
	public:
		Listener(QOgreWidgetPrivate *);

		virtual void preRenderTargetUpdate(const Ogre::RenderTargetEvent & event);
		virtual void postRenderTargetUpdate(const Ogre::RenderTargetEvent & event);

	private:
		QOgreWidgetPrivate * target;
	};

	QOGRE_OBJECT_PRIVATE(QOgreWidget)

	QOgreWidgetPrivate(QOgreWidget *);
	~QOgreWidgetPrivate();

	bool isValid() const;

	void initialize();
	void shutdown();
	void resize(const QSize &);

	void createWindow();
	void destroyWindow();

	static QString windowHandle(QWidget *);

	QOgreWidget * q_ptr;

	bool initialized;

	QPointer<QOgreCamera> camera;
	Ogre::RenderWindow * window;
	Ogre::Viewport * viewport;
	Listener listener;
	bool autoUpdate;
};


inline bool QOgreWidgetPrivate::isValid() const
{
	return initialized && window && viewport;
}

#endif // QOGREWIDGET_P_H
