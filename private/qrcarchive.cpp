#include "qrcarchive.h"
#include <QString>
#include <QVector>
#include <QFileInfo>
#include <QDir>
#include <QDateTime>

using namespace Ogre;

QRCArchiveFactory::QRCArchiveFactory()
{
}

QRCArchiveFactory::~QRCArchiveFactory()
{
}

const Ogre::String & QRCArchiveFactory::getType() const
{
	return QRCArchive::ResourceType;
}

Ogre::Archive * QRCArchiveFactory::createInstance(const Ogre::String & name, bool readOnly)
{
	if (!readOnly)
		return NULL;

	return new QRCArchive(name, QRCArchive::ResourceType);
}

void QRCArchiveFactory::destroyInstance(Ogre::Archive * archive)
{
	delete archive;
}

QRCArchiveFactory * QRCArchiveFactory::instance()
{
	static QRCArchiveFactory object;
	return &object;
}

// ---------------------------------------------------------------------------------------------------------------------------- //

const String QRCArchive::ResourceType("QRC");

QRCArchive::QRCArchive(const String & name, const String & type)
	: Archive(name, type)
{
}

QRCArchive::~QRCArchive()
{
}

bool QRCArchive::isCaseSensitive() const
{
	return true;
}

void QRCArchive::load()
{
}

void QRCArchive::unload()
{
}

bool QRCArchive::isReadOnly()
{
	return true;
}

DataStreamPtr QRCArchive::open(const String & fileName, bool readOnly) const
{
	Q_UNUSED(readOnly);

	QRCDataStream * stream = new QRCDataStream(qualifiedFileName(fileName));
	return DataStreamPtr(!stream->open() ? NULL : stream);
}

StringVectorPtr QRCArchive::list(bool recursive, bool dirs)
{
	QFileInfoList files = listFiles(QString::fromStdString(getName()), dirs ? CollectDirectories : CollectFiles, recursive);

	StringVectorPtr result(new StringVector);
	foreach (const QFileInfo info, files)
		result->push_back(info.filePath().toStdString());

	return result;
}

FileInfoListPtr QRCArchive::listFileInfo(bool recursive, bool dirs)
{
	QFileInfoList files = listFiles(QString::fromStdString(getName()), dirs ? CollectDirectories : CollectFiles, recursive);

	FileInfoListPtr result(new FileInfoList);
	foreach (const QFileInfo info, files)
		result->push_back(toOgreFileInfo(info));

	return result;
}

StringVectorPtr QRCArchive::find(const String & pattern, bool recursive, bool dirs)
{
	QFileInfoList files = listFiles(QString::fromStdString(getName()), dirs ? CollectDirectories : CollectFiles, recursive, QString::fromStdString(pattern));

	StringVectorPtr result(new StringVector);

	foreach (const QFileInfo info, files)
		result->push_back(info.filePath().toStdString());

	return result;
}

bool QRCArchive::exists(const String & file)
{
	return QFile::exists(qualifiedFileName(file));
}

time_t QRCArchive::getModifiedTime(const String & file)
{
	return QFileInfo(qualifiedFileName(file)).lastModified().toTime_t();
}

FileInfoListPtr QRCArchive::findFileInfo(const String & pattern, bool recursive, bool dirs) const
{
	QFileInfoList files = listFiles(QString::fromStdString(getName()), dirs ? CollectDirectories : CollectFiles, recursive, QString::fromStdString(pattern));

	FileInfoListPtr result(new FileInfoList);

	foreach (const QFileInfo info, files)
		result->push_back(toOgreFileInfo(info));

	return result;
}

QFileInfoList QRCArchive::listFiles(const QString & path, CollectType collect, bool recursive, const QString & nameFilter) const
{
	QDir dir(path);

	QDir::Filters filter;
	if (collect == CollectDirectories)
		filter = QDir::Dirs | QDir::NoDotAndDotDot;
	else if (recursive)
		filter = QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot;
	else
		filter = QDir::Files;

	QFileInfoList list = nameFilter.isEmpty() ? dir.entryInfoList(filter, QDir::DirsFirst) : dir.entryInfoList(QStringList(nameFilter), filter, QDir::DirsFirst);

	if (recursive)  {
		// Collect the files / dirs recursively
		foreach (const QFileInfo file, list)  {
			if (!file.isDir())
				break;

			list += listFiles(dir.filePath(file.fileName()), collect, recursive);
		}
	}

	return list;
}

FileInfo QRCArchive::toOgreFileInfo(const QFileInfo & info) const
{
	FileInfo fileInfo = {
        this,
		info.filePath().toStdString(),
		info.dir().path().toStdString(),
		info.baseName().toStdString(),
		static_cast<size_t>(info.size()),
		static_cast<size_t>(info.size())
	};

	return fileInfo;
}

QString QRCArchive::qualifiedFileName(const String & file) const
{
	return QDir(QString::fromStdString(getName())).filePath(QString::fromStdString(file));
}

// ---------------------------------------------------------------------------------------------------------------------------- //

QRCDataStream::QRCDataStream(const QString & filePath)
	: file(filePath), stream(&file)
{
}

QRCDataStream::~QRCDataStream()
{
}

bool QRCDataStream::open()
{
	return file.open(QFile::ReadOnly);
}

void QRCDataStream::close()
{
	file.close();
}

size_t QRCDataStream::read(void * buffer, size_t count)
{
	return stream.readRawData(static_cast<char *>(buffer), count);
}

void QRCDataStream::skip(long count)
{
	stream.skipRawData(count);
}

void QRCDataStream::seek(size_t pos)
{
	file.seek(pos);
}

size_t QRCDataStream::tell() const
{
	return file.pos();
}

bool QRCDataStream::eof() const
{
	return stream.atEnd();
}
