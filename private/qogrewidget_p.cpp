#include "qogrewidget_p.h"
#include "qogreobject.h"
#include "qogrecamera.h"
#include "qogrescene.h"
#include "qogrecast.h"
#include "qogreengine.h"
#include "qogrelog.h"

#include <QString>
#include <QApplication>
#include <QDesktopWidget>

#include <QWindow>

#if defined(Q_OS_LINUX)
#include <QX11Info>
#endif

#if !defined(Q_OS_WIN32) && !defined(Q_OS_LINUX) && !defined(Q_OS_MAC)
	#error "You're trying to compile QOgre with an unsupported window manager."
#endif


/*

void QOgreEngine::startRendering()
{
	QOGRE_D(QOgreEngine);
	if (d->renderRequests <= 0)  {
		emit renderingStarted();

		Ogre::WindowEventUtilities::messagePump();
		d->root->_fireFrameStarted();
	}
	d->renderRequests++;
}

void QOgreEngine::finishRendering()
{
	QOGRE_D(QOgreEngine);
	d->renderRequests--;
	if (d->renderRequests <= 0)  {
		for (Ogre::SceneManagerEnumerator::SceneManagerIterator it = d->root->getSceneManagerIterator(); it.hasMoreElements(); it.moveNext())
			it.peekNextValue()->_handleLodEvents();

		d->root->_fireFrameEnded();
		d->renderingQueued = false;

		emit renderingFinished();
	}
}

void QOgreEngine::queueRendering()
{
	QOGRE_D(QOgreEngine);
	if (d->renderRequests > 0 && !d->renderingQueued)  {
		d->renderingQueued = true;
		d->root->_fireFrameRenderingQueued();
		emit rendering();
	}
}*/

QOgreWidgetPrivate::Listener::Listener(QOgreWidgetPrivate * widget)
	: target(widget)
{
}

void QOgreWidgetPrivate::Listener::preRenderTargetUpdate(const Ogre::RenderTargetEvent & event)
{
	target->q()->renderingStarted();
}

void QOgreWidgetPrivate::Listener::postRenderTargetUpdate(const Ogre::RenderTargetEvent & event)
{
	target->q()->renderingFinished();
}

QOgreWidgetPrivate::QOgreWidgetPrivate(QOgreWidget * q)
	: q_ptr(q), initialized(false), camera(NULL), window(NULL), viewport(NULL), listener(this), autoUpdate(true)
{
}

QOgreWidgetPrivate::~QOgreWidgetPrivate()
{
}

void QOgreWidgetPrivate::initialize()
{
	// Initialize the graphics context if needed
	if (initialized)
		return;
	initialized = true;

	try  {
		createWindow();
	}
	catch (Ogre::Exception e)
	{
		QOgreLog::warning(QString("QOgreWidget failed to initialize."));
		QOgreLog::error(QString("Ogre exception: %1").arg(e.what()));
	}
}

void QOgreWidgetPrivate::shutdown()
{
	q()->destroy(false, true);
	destroyWindow();			// Destroy the Ogre window
}

void QOgreWidgetPrivate::createWindow()
{
	QOGRE_Q(QOgreWidget);

	// Generate the window name
	static qint32 windowIndex = 0;
	Ogre::String windowName(QString("QOgre/RenderWindow#%1").arg(windowIndex++).toStdString());

	Ogre::NameValuePairList windowParameters;

	QWidget * parent = q->nativeParentWidget();
	if (!parent)
		parent = QApplication::desktop();

//	windowParameters["parentWindowHandle"] = windowHandle(q).toStdString();
	windowParameters["externalWindowHandle"] = windowHandle(q).toStdString();
	windowParameters["FSAAHint"] = "Quality";

	Ogre::Root & root = Ogre::Root::getSingleton();

	// Create a render window
	try  {
		window = root.createRenderWindow(windowName, q->width(), q->height(), false, &windowParameters);
	}
	catch (...)
	{
		Q_ASSERT(false);
		return;
	}

	window->setVisible(true);
	window->setActive(true);
	window->setAutoUpdated(true);

#if !defined(Q_OS_MAC)
	WId id;
	window->getCustomAttribute("WINDOW", &id);
	Q_ASSERT(id);

	QRect geometry = q->geometry();
	q->create(id, true, true);
	q->setGeometry(geometry);
#else
	q->makeCurrent();		// For MAC make the window the current
#endif

	window->addListener(&listener);

	emit q->ready(q);
}

void QOgreWidgetPrivate::destroyWindow()
{
	if (!window)
		return;

	window->removeListener(&listener);

	if (viewport)
		window->removeViewport(viewport->getZOrder());

	window->destroy();
	window = NULL;
}

void QOgreWidgetPrivate::resize(const QSize & size)
{
	qint32 width = size.width(), height = size.height();

	window->resize(width, height);

	if (camera)
		qogre_cast(camera)->setAspectRatio(static_cast<qreal>(width) / height);

	window->windowMovedOrResized();
}

QString QOgreWidgetPrivate::windowHandle(QWidget * widget)
{
#if defined(Q_OS_MAC) || defined(Q_OS_WIN32)
	return QString("%1").arg(widget->windowHandle()->winId());
#elif defined(Q_OS_LINUX)
	static quintptr display = reinterpret_cast<quintptr>(QX11Info::display()), screen = QApplication::desktop()->primaryScreen();
	return QString("%1:%2:%3").arg(display).arg(screen).arg(static_cast<quintptr>(widget->windowHandle()->winId()));
#endif
}
