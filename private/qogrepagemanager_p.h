#ifndef QOGREPAGING_P_H
#define QOGREPAGING_P_H

#include "qogreobject_p.h"
#include <Paging/OgrePageManager.h>

class QOgrePageManager;
class QOGRE_LOCAL QOgrePageManagerPrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgrePageManager)

public:
	QOgrePageManagerPrivate(QOgrePageManager *);
	virtual ~QOgrePageManagerPrivate();

private:
	Ogre::PageManager * pageManager;
};

#endif // QOGREPAGING_P_H
