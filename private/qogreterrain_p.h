#ifndef QOGRETERRAIN_P_H
#define QOGRETERRAIN_P_H

#include "qogreobject_p.h"
#include "qogreterrainoptions.h"
#include "qogreterrainlayer.h"

#include <QSize>
#include <QList>
#include <QVector>
#include <QVector3D>
#include <QPointer>

#include <Terrain/OgreTerrainGroup.h>
#include <Terrain/OgreTerrainPaging.h>
#include <Terrain/OgreTerrainPagedWorldSection.h>

class QOgreWorld;
class QOgreLight;
class QOgreTerrainTile;
class QOgreWorld;

class QOgreTerrain;
class QOGRE_LOCAL QOgreTerrainPrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreTerrain)

	class PageProvider : public Ogre::PageProvider
	{
	public:
		virtual bool prepareProceduralPage(Ogre::Page *, Ogre::PagedWorldSection *);
		virtual bool loadProceduralPage(Ogre::Page *, Ogre::PagedWorldSection *);
		virtual bool unloadProceduralPage(Ogre::Page *, Ogre::PagedWorldSection *);
		virtual bool unprepareProceduralPage(Ogre::Page *, Ogre::PagedWorldSection *);
	};

	class TerrainDefiner : public Ogre::TerrainPagedWorldSection::TerrainDefiner
	{
		virtual void define(Ogre::TerrainGroup * terrainGroup, long x, long y);
		virtual ~TerrainDefiner();
	};

	QOgreTerrainPrivate(QOgreTerrain *, QOgreWorld *);
	~QOgreTerrainPrivate();

	void initialize();

	QPointer<QOgreWorld> world;
	QOgreTerrainOptions options;
	QtOgre::PrincipalPlanes alignment;
	QSize terrainSize;		// In terrain tiles
	quint16 tileSize;			//
	qreal worldSize;
	QVector3D origin;
	qint8 lod;
	QPointer<QOgreLight> light;
	QList<QOgreTerrainLayer> layers;
	QVector<QOgreTerrainTile *> tiles;
	// Supports terrain paging
	QSize pageSize;
	Ogre::TerrainGroup * terrainGroup;
	Ogre::TerrainPaging * terrainPaging;
	PageProvider pageProvider;
};

#endif // QOGRETERRAIN_P_H
