#include "qogredirectionallight_p.h"
#include "qogredirectionallight.h"

QOgreDirectionalLightPrivate::QOgreDirectionalLightPrivate(Ogre::SceneManager * scene, Ogre::SceneNode * parentNode, QOgreLight * q)
	: QOgreLightPrivate(scene, parentNode, q)
{
}
