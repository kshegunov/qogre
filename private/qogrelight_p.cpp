#include "qogrelight_p.h"
#include "qogrelight.h"

#include <QString>

QOgreLightPrivate::QOgreLightPrivate(Ogre::SceneManager * manager, Ogre::SceneNode * parentNode, QOgreLight * q)
	: QOgreObjectPrivate(q), light(NULL), node(NULL), scene(manager)
{
	Q_ASSERT(scene);

	createLight();
	createNode(parentNode);
}

QOgreLightPrivate::~QOgreLightPrivate()
{
	Q_ASSERT(scene);

	destroyNode();
	destroyLight();
}

inline void QOgreLightPrivate::createLight()
{
	try  {
		static qint32 lightIndex = 0;
		light = scene->createLight(QString("QOgre/QOgreLight#%1").arg(lightIndex++).toStdString());
	}
	catch (...)
	{
		destroyLight();
	}
}

inline void QOgreLightPrivate::destroyLight()
{
	if (!light)
		return;

	scene->destroyLight(light);
	light = NULL;
}

inline void QOgreLightPrivate::createNode(Ogre::SceneNode * parentNode)
{
	if (!light)
		return;

	try  {
		static qint32 lightNodeIndex = 0;
		node = parentNode->createChildSceneNode(QString("QOgre/QOgreLight/OgreSceneNode#%1").arg(lightNodeIndex++).toStdString());
		node->attachObject(light);
	}
	catch (...)
	{
		destroyNode();
	}
}

inline void QOgreLightPrivate::destroyNode()
{
	if (!node)
		return;

	node->detachAllObjects();
	node->getParentSceneNode()->removeChild(node->getName());
	scene->destroySceneNode(node);
	node = NULL;
}


