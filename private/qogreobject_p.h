#ifndef QOGREOBJECT_P_H
#define QOGREOBJECT_P_H

#include "qogre.h"

class QOgreObject;
class QOGRE_LOCAL QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreObject)

public:
	QOgreObjectPrivate(QOgreObject *);
	virtual ~QOgreObjectPrivate();

protected:
	QOgreObject * q_ptr;
};

#endif // QOGREOBJECT_P_H
