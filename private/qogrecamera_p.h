#ifndef QOGRECAMERA_P_H
#define QOGRECAMERA_P_H

#include "qogre.h"
#include "qogrenode_p.h"

#include <QVector3D>
#include <QQuaternion>

#include <OgreCamera.h>

class QOgreCamera;
class QOGRE_LOCAL QOgreCameraPrivate : public QOgreNodePrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreCamera)

public:
	QOgreCameraPrivate(QOgreScene * scene, QOgreCamera * q);
	virtual ~QOgreCameraPrivate();

	bool isValid() const;
	void attachAnchor(QOgreNode *);
	QOgreNode * detachAnchor();

	void rotate(qreal angle, const QVector3D & axis);

private:
	QPointer<QOgreNode> anchor;
	Ogre::Camera * camera;
};

inline bool QOgreCameraPrivate::isValid() const
{
	return camera && node;
}


#endif // QOGRECAMERA_P_H
