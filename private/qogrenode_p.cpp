#include "qogrenode_p.h"
#include "qogrescene.h"
#include "qogrecast.h"

#include <OgreSceneManager.h>

QOgreNodePrivate::QOgreNodePrivate(QOgreScene * scene, QOgreObject * q)
	: QOgreObjectPrivate(q), position(positionData), orientation(orientationData)
{
	node = qogre_cast(scene)->getRootSceneNode()->createChildSceneNode();

	positionData.setNode(node);
	orientationData.setNode(node);
}

QOgreNodePrivate::QOgreNodePrivate(QOgreNode * parent, QOgreObject * q)
	: QOgreObjectPrivate(q), position(positionData), orientation(orientationData)
{
	node = qogre_cast(parent)->createChildSceneNode();

	positionData.setNode(node);
	orientationData.setNode(node);
}

QOgreNodePrivate::~QOgreNodePrivate()
{
	node->detachAllObjects();
	node->getParentSceneNode()->removeChild(node->getName());
	node->getCreator()->destroySceneNode(node);

	node = NULL;
	positionData.setNode(NULL);
	orientationData.setNode(NULL);
}
