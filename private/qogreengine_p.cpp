#include "qogreengine_p.h"
#include "qogreengine.h"
#include "qogrerenderer.h"
#include "qogrerenderer_p.h"
#include "qogrelog.h"
#include "qogreexception.h"
#include "qogrecast.h"
#include "qrcarchive.h"

#include <QLibrary>
#include <QApplication>
#include <QDesktopWidget>
#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>

#include <OgrePlugin.h>
#ifdef QOGRE_STATIC_GL
#include <OgreGLPlugin.h>
#endif
#ifdef QOGRE_STATIC_CG
#include <OgreCgPlugin.h>
#endif
#ifdef QOGRE_STATIC_CODECS
#include <FreeImage.h>
#include <OgreDDSCodec.h>
#include <OgreFreeImageCodec.h>
#endif

const QString QOgreEnginePrivate::PLUGINS_PREFIX("QOgrePlugins");

QOgreEnginePrivate::QOgreEnginePrivate(QOgreEngine * q)
	: QOgreObjectPrivate(q), root(new Ogre::Root("", "", "")), resourcesInitialized(false)
{
	Ogre::Log * log = Ogre::LogManager::getSingleton().createLog("", true, true, true);
	log->setLogDetail(Ogre::LL_NORMAL);
}

QOgreEnginePrivate::~QOgreEnginePrivate()
{
	try  {
		// This will gloriously crash if the render system is reinitialized at some point.
		// The issue is related to the following ogre bug: https://ogre3d.atlassian.net/browse/OGRE-17 (fixed in v. 1.9)
		// Free the root and all associated resources
		delete root;
		root = NULL;

		// Delete the manually installed plugins
		foreach (Ogre::Plugin * plugin, plugins)
			delete plugin;
	}
	catch (...)
	{
	}
}

void QOgreEnginePrivate::loadPlugins()
{
	static bool pluginsLoaded = false;
	if (pluginsLoaded)
		return;
	pluginsLoaded = true;

	QDir dir(PLUGINS_PREFIX + ':');

	QFileInfoList files = dir.entryInfoList(QDir::Files | QDir::Readable);
	foreach (const QFileInfo file, files)  {
		if (!QLibrary::isLibrary(file.filePath()) || !requiredPlugins.contains(file.baseName()))
			continue;

		// Try loading the required ogre plugin
		try  {
			root->loadPlugin(dir.filePath(file.fileName()).toStdString());
		}
		catch (Ogre::Exception e)
		{
			QOgreLog::warning(QString("Plugin \"%1\" was not loaded.").arg(file.baseName()));
			QOgreLog::error(QString("Ogre exception: %1").arg(e.what()));
		}
	}

#ifdef QOGRE_STATIC_GL
	plugins << new Ogre::GLPlugin();
#endif
#ifdef QOGRE_STATIC_CG
	plugins << new Ogre::CgPlugin();
#endif

	foreach (Ogre::Plugin * plugin, plugins)
		root->installPlugin(plugin);

#ifdef QOGRE_STATIC_CODECS
	FreeImage_Initialise();
	Ogre::DDSCodec::startup();
	Ogre::FreeImageCodec::startup();
#endif

	QOgreRendererPrivate::loadRenderers();

	emit q()->pluginsLoaded();
}

void QOgreEnginePrivate::initialize()
{
	loadPlugins();

	static bool initialized = false;
	if (initialized)
		return;
	initialized = true;

	if (!root->getRenderSystem())  {
		// Have to select a render system before proceeding - try the first one
		QOgreRendererList renderers = QOgreRenderer::renderers();
		if (renderers.isEmpty())
			throw QOgreInitializationException("No render system is available");

		Q_ASSERT(dynamic_cast<QApplication *>(QCoreApplication::instance()));
		QDesktopWidget * desktop = reinterpret_cast<QApplication *>(QApplication::instance())->desktop();
		QOgreRenderer * renderer = renderers.first();
		renderer->setResolution(desktop->width(), desktop->height());
		renderer->select();
	}

	root->initialise(false);

	pageManager = new QOgrePageManager(q());
}

void QOgreEnginePrivate::addResourceLocation(const QString & location, QtOgre::ResourceType type)
{
	// Check if that resource was already loaded
	QString fullLocation = QString("%1::%2").arg(qogre_cast(type)).arg(location);
	if (loadedResources.contains(fullLocation))
		return;

	loadedResources.insert(fullLocation);

	Ogre::ResourceGroupManager & resourceManager = Ogre::ResourceGroupManager::getSingleton();

	static qint32 resourceGroupCounter = 0;
	Ogre::String groupName(QString("QOgre/QOgreResourceGroup#%1").arg(resourceGroupCounter++).toStdString());

	resourceManager.addResourceLocation(location.toStdString(), qogre_cast(type).toStdString(), groupName);
	if (resourcesInitialized)
		resourceManager.initialiseResourceGroup(groupName);
}

bool QOgreEnginePrivate::loadAllResources()
{
	// Loads the resources (if not already done)
	if (resourcesInitialized)
		return false;

	resourcesInitialized = true;

	Ogre::ArchiveManager::getSingleton().addArchiveFactory(Ogre::QRCArchiveFactory::instance());
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	return true;
}
