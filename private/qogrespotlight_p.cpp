#include "qogrespotlight_p.h"
#include "qogrespotlight.h"

QOgreSpotLightPrivate::QOgreSpotLightPrivate(Ogre::SceneManager * scene, Ogre::SceneNode * parentNode, QOgreLight * q)
	: QOgreLightPrivate(scene, parentNode, q), position(positionData), orientation(orientationData)
{
	positionData.setNode(node);
	orientationData.setNode(node);
}

QOgreSpotLightPrivate::~QOgreSpotLightPrivate()
{
	positionData.setNode(NULL);
	orientationData.setNode(NULL);
}
