#ifndef QOGRELOG_H
#define QOGRELOG_H

#include "qogre.h"

#include <QTextStream>

class QString;

class QOGRE_LOCAL QOgreLog
{
public:
	~QOgreLog();

	static void warning(const QString &);
	static void error(const QString &);
	static void notice(const QString &);

private:
	QOgreLog();

	inline static QOgreLog & instance();

	QTextStream cerr;
};

#endif // QOGRELOG_H
