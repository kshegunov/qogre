#ifndef QOGRESCENE_P_H
#define QOGRESCENE_P_H

#include "qogreobject_p.h"

#include <OgreSceneManager.h>

class QOgreScene;
class QOGRE_LOCAL QOgreScenePrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreScene)

public:
	QOgreScenePrivate(Ogre::SceneTypeMask, QOgreScene *);
	virtual ~QOgreScenePrivate();

private:
	Ogre::SceneManager * scene;
	Ogre::SceneTypeMask type;
};

#endif // QOGRESCENE_P_H
