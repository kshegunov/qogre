#ifndef QOGRENODE_P_H
#define QOGRENODE_P_H

#include "qogreobject_p.h"
#include "qogreposition.h"
#include "qogreposition_p.h"
#include "qogreorientation.h"
#include "qogreorientation_p.h"
#include "qogrescene.h"

#include <QPointer>

#include <OgreSceneNode.h>

class QOgreNode;
class QOGRE_LOCAL QOgreNodePrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreNode)

protected:
	QOgreNodePrivate(QOgreScene *, QOgreObject *);
	QOgreNodePrivate(QOgreNode *, QOgreObject *);
	virtual ~QOgreNodePrivate();

	QOgrePositionPrivate positionData;
	QOgrePosition position;
	QOgreOrientationPrivate orientationData;
	QOgreOrientation orientation;

	Ogre::SceneNode * node;
};

#endif // QOGRENODE_P_H
