#ifndef QOGRECAST_H
#define QOGRECAST_H

#include "qogreengine.h"
#include "qogreengine_p.h"
#include "qogrewidget.h"
#include "qogrescene.h"
#include "qogrecamera.h"
#include "qogremesh.h"
#include "qogrewidget_p.h"
#include "qogrescene_p.h"
#include "qogrecamera_p.h"
#include "qogremesh_p.h"
#include "qogrelight.h"
#include "qogrelight_p.h"
#include "qogreposition.h"
#include "qogrerenderer.h"
#include "qogrerenderer_p.h"
#include "qogreterrain.h"
#include "qogreterrain_p.h"
#include "qogrepagemanager.h"
#include "qogrepagemanager_p.h"
#include "qogreworld.h"
#include "qogreworld_p.h"

#include <Ogre.h>

#include <QColor>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QQuaternion>

class QOGRE_LOCAL QOgreCast
{
public:
	Ogre::Root * operator() (QOgreEngine *) const;

	Ogre::RenderWindow * operator () (QOgreWidget *) const;
	Ogre::SceneManager * operator () (QOgreScene *) const;
	Ogre::Entity * operator () (QOgreMesh *) const;
	Ogre::Light * operator () (QOgreLight *) const;
	Ogre::RenderSystem * operator () (QOgreRenderer *) const;
	Ogre::Camera * operator () (QOgreCamera *) const;
	Ogre::SceneNode * operator () (QOgreNode *) const;
	Ogre::PageManager * operator() (QOgrePageManager *) const;
	Ogre::PagedWorld * operator () (QOgreWorld *) const;
	const Ogre::RenderWindow * operator () (const QOgreWidget *) const;
	const Ogre::SceneManager * operator () (const QOgreScene *) const;
	const Ogre::Entity * operator () (const QOgreMesh *) const;
	const Ogre::Light * operator () (const QOgreLight *) const;
	const Ogre::RenderSystem * operator () (const QOgreRenderer *) const;
	const Ogre::Camera * operator () (const QOgreCamera *) const;
	const Ogre::SceneNode * operator () (const QOgreNode *) const;
	const Ogre::PageManager * operator() (const QOgrePageManager *) const;
	const Ogre::PagedWorld * operator () (const QOgreWorld *) const;
	// Colors
	Ogre::ColourValue operator () (const QColor &) const;
	QColor operator () (const Ogre::ColourValue &) const;
	// Vectors
	Ogre::Vector2 operator () (const QVector2D &) const;
	Ogre::Vector3 operator () (const QVector3D &) const;
	Ogre::Vector4 operator () (const QVector4D &) const;
	Ogre::Quaternion operator () (const QQuaternion &) const;
	QVector2D operator () (const Ogre::Vector2 &) const;
	QVector3D operator () (const Ogre::Vector3 &) const;
	QVector4D operator () (const Ogre::Vector4 &) const;
	QQuaternion operator () (const Ogre::Quaternion &) const;
	// Enumerators
	Ogre::Node::TransformSpace operator () (PositionType) const;
	QString operator() (const QtOgre::ResourceType) const;
	Ogre::SceneTypeMask operator () (QOgreExteriorScene::SceneType) const;
	Ogre::ShadowTechnique operator () (const QtOgre::ShadowType) const;
	QtOgre::ShadowType operator () (const Ogre::ShadowTechnique) const;
	RendererOptionList operator() (const RenderOptions) const;
	Ogre::Capabilities operator() (const QtOgre::RenderFeatures) const;
	Ogre::Terrain::Alignment operator() (const QtOgre::PrincipalPlanes) const;
};

extern const QOgreCast qogre_cast;

inline Ogre::Root * QOgreCast::operator() (QOgreEngine * engine) const
{
	return engine->d()->root;
}

inline Ogre::RenderWindow * QOgreCast::operator () (QOgreWidget * widget) const
{
	return widget->d()->window;
}

inline Ogre::SceneManager * QOgreCast::operator () (QOgreScene * scene) const
{
	return scene->d()->scene;
}

inline Ogre::Entity * QOgreCast::operator () (QOgreMesh * mesh) const
{
	return mesh->d()->entity;
}

inline Ogre::Light * QOgreCast::operator () (QOgreLight * light) const
{
	return light->d()->light;
}

inline Ogre::RenderSystem * QOgreCast::operator () (QOgreRenderer * renderer) const
{
	return renderer->d()->renderer;
}

inline Ogre::Camera * QOgreCast::operator () (QOgreCamera * camera) const
{
	return camera->d()->camera;
}

inline Ogre::SceneNode * QOgreCast::operator () (QOgreNode * node) const
{
	return node->d()->node;
}

inline Ogre::PageManager * QOgreCast::operator() (QOgrePageManager * paging) const
{
	return paging->d()->pageManager;
}

inline Ogre::PagedWorld * QOgreCast::operator() (QOgreWorld * world) const
{
	return world->d()->world;
}

inline const Ogre::RenderWindow * QOgreCast::operator () (const QOgreWidget * widget) const
{
	return widget->d()->window;
}

inline const Ogre::SceneManager * QOgreCast::operator () (const QOgreScene * scene) const
{
	return scene->d()->scene;
}

inline const Ogre::Entity * QOgreCast::operator () (const QOgreMesh * mesh) const
{
	return mesh->d()->entity;
}

inline const Ogre::Light * QOgreCast::operator () (const QOgreLight * light) const
{
	return light->d()->light;
}

inline const Ogre::RenderSystem * QOgreCast::operator () (const QOgreRenderer * renderer) const
{
	return renderer->d()->renderer;
}

inline const Ogre::Camera * QOgreCast::operator () (const QOgreCamera * camera) const
{
	return camera->d()->camera;
}

inline const Ogre::SceneNode * QOgreCast::operator () (const QOgreNode * node) const
{
	return node->d()->node;
}

inline const Ogre::PageManager * QOgreCast::operator () (const QOgrePageManager * paging) const
{
	return paging->d()->pageManager;
}

inline const Ogre::PagedWorld * QOgreCast::operator() (const QOgreWorld * world) const
{
	return world->d()->world;
}

inline Ogre::ColourValue QOgreCast::operator () (const QColor & color) const
{
	return Ogre::ColourValue(color.redF(), color.greenF(), color.blueF(), color.alphaF());
}

inline QColor QOgreCast::operator () (const Ogre::ColourValue & value) const
{
	return QColor::fromRgbF(value.r, value.g, value.b, value.a);
}

inline Ogre::Vector2 QOgreCast::operator () (const QVector2D & vector) const
{
	return Ogre::Vector2(vector.x(), vector.y());
}

inline Ogre::Vector3 QOgreCast::operator () (const QVector3D & vector) const
{
	return Ogre::Vector3(vector.x(), vector.y(), vector.z());
}

inline Ogre::Vector4 QOgreCast::operator () (const QVector4D & vector) const
{
	return Ogre::Vector4(vector.x(), vector.y(), vector.z(), vector.w());
}

inline Ogre::Quaternion QOgreCast::operator () (const QQuaternion & quaternion) const
{
	return Ogre::Quaternion(quaternion.scalar(), quaternion.x(), quaternion.y(), quaternion.z());
}

inline QVector2D QOgreCast::operator () (const Ogre::Vector2 & vector) const
{
	return QVector2D(vector.x, vector.y);
}

inline QVector3D QOgreCast::operator () (const Ogre::Vector3 & vector) const
{
	return QVector3D(vector.x, vector.y, vector.z);
}

inline QVector4D QOgreCast::operator () (const Ogre::Vector4 & vector) const
{
	return QVector4D(vector.x, vector.y, vector.z, vector.w);
}

inline QQuaternion QOgreCast::operator () (const Ogre::Quaternion & quaternion) const
{
	return QQuaternion(quaternion.w, quaternion.x, quaternion.y, quaternion.z);
}




#endif // QOGRECAST_H
