#ifndef QOGREPOSITION_P_H
#define QOGREPOSITION_P_H

#include "qogre.h"

#include <OgreNode.h>

class QOgrePosition;
class QOGRE_LOCAL QOgrePositionPrivate
{
	friend class QOgrePosition;

public:
	explicit QOgrePositionPrivate(Ogre::Node * = NULL);

	void setNode(Ogre::Node *);

private:
	Ogre::Node * node;
};

inline QOgrePositionPrivate::QOgrePositionPrivate(Ogre::Node * ogreNode)
	: node(ogreNode)
{
}

inline void QOgrePositionPrivate::setNode(Ogre::Node * ogreNode)
{
	node = ogreNode;
}


#endif // QOGREPOSITION_P_H
