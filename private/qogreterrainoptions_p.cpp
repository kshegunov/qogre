#include "qogreterrainoptions_p.h"

qint32 QOgreTerrainOptionsPrivate::reference = 0;

QOgreTerrainOptionsPrivate::QOgreTerrainOptionsPrivate()
{
	Ogre::TerrainGlobalOptions * opt = Ogre::TerrainGlobalOptions::getSingletonPtr();
	options = opt ? opt : new Ogre::TerrainGlobalOptions;
	reference++;
}

QOgreTerrainOptionsPrivate::~QOgreTerrainOptionsPrivate()
{
	reference--;
	if (reference <= 0)
		delete options;
}

