#ifndef QOGRETERRAINOPTIONSPRIVATE_H
#define QOGRETERRAINOPTIONSPRIVATE_H

#include <QtGlobal>
#include <Terrain/OgreTerrain.h>

class QOgreTerrainOptions;
class QOgreTerrainOptionsPrivate
{
	friend class QOgreTerrainOptions;

public:
	QOgreTerrainOptionsPrivate();
	~QOgreTerrainOptionsPrivate();

private:
	Ogre::TerrainGlobalOptions * options;
	static qint32 reference;
};

#endif // QOGRETERRAINOPTIONSPRIVATE_H
