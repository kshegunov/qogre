#ifndef QOGREDIRECTIONALLIGHT_P_H
#define QOGREDIRECTIONALLIGHT_P_H

#include "qogrelight_p.h"

class QOgreDirectionalLight;
class QOGRE_LOCAL QOgreDirectionalLightPrivate : public QOgreLightPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreDirectionalLight)

	QOgreDirectionalLightPrivate(Ogre::SceneManager *, Ogre::SceneNode *, QOgreLight *);
};

#endif // QOGREDIRECTIONALLIGHT_P_H
