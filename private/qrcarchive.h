#ifndef QRCARCHIVE_H
#define QRCARCHIVE_H

#include "qogre.h"

#include <OgreArchive.h>
#include <OgreArchiveFactory.h>
#include <OgreDataStream.h>

#include <QFile>
#include <QDataStream>

class QString;
class QFileInfo;
template <class T>
class QList;
typedef QList<QFileInfo> QFileInfoList;

namespace Ogre
{
	class QOGRE_LOCAL QRCArchiveFactory : public ArchiveFactory
	{
		QRCArchiveFactory();

	public:
		virtual ~QRCArchiveFactory();

		static QRCArchiveFactory * instance();

		virtual const String & getType() const;
		virtual Archive * createInstance(const String & name, bool readOnly);
		virtual void destroyInstance(Archive *);
	};

	class QOGRE_LOCAL QRCArchive : public Archive
	{
		friend class QRCArchiveFactory;

	public:
		static const String ResourceType;

	private:
		QRCArchive(const String &, const String &);
	public:
		virtual ~QRCArchive();

		virtual bool isCaseSensitive() const;
		virtual void load();
		virtual void unload();
		virtual bool isReadOnly();
        virtual DataStreamPtr open(const String &, bool = true) const;
		virtual StringVectorPtr list(bool = true, bool = false);
		virtual FileInfoListPtr listFileInfo(bool = true, bool = false);
		virtual StringVectorPtr find(const String &, bool = true, bool = false);
		virtual bool exists(const String &);
		virtual time_t getModifiedTime(const String &);
        virtual FileInfoListPtr findFileInfo(const String &, bool = true, bool = false) const;

	private:
		enum CollectType { CollectFiles, CollectDirectories };
		QFileInfoList listFiles(const QString &, CollectType, bool, const QString & = QString()) const;
        inline FileInfo toOgreFileInfo(const QFileInfo &) const;
		inline QString qualifiedFileName(const String &) const;
	};

	class QOGRE_LOCAL QRCDataStream : public DataStream
	{
		friend class QRCArchive;

		QRCDataStream(const QString &);
	public:
		virtual ~QRCDataStream();

		bool open();
		virtual void close();

		virtual size_t read(void *, size_t);
		virtual void skip(long);
		virtual void seek(size_t);
		virtual size_t tell() const;
		virtual bool eof() const;

	private:
		QFile file;
		QDataStream stream;
	};
}

#endif // QRCARCHIVE_H
