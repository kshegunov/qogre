#ifndef QOGREWORLD_P_H
#define QOGREWORLD_P_H

#include "qogreobject_p.h"
#include "qogrepagemanager.h"
#include "qogrescene.h"
#include "qogreterrain.h"

#include <QPointer>
#include <QHash>

#include <Paging/OgrePagedWorld.h>

class QOgreWorld;
class QOGRE_LOCAL QOgreWorldPrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreWorld)

	typedef QHash<QString, QOgreWorld *> QOgreWorldsHash;

public:
	QOgreWorldPrivate(const QString &, QOgreWorld *);
	virtual ~QOgreWorldPrivate();

private:
	void changeName(const QString &);

	QString name;
	QPointer<QOgreScene> scene;
	QPointer<QOgreTerrain> terrain;
	Ogre::PagedWorld * world;

	static qint32 worldsIndex;
	static QOgreWorldsHash worlds;
};

#endif // QOGREWORLD_P_H
