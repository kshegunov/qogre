#include "qogrepointlight_p.h"
#include "qogrepointlight.h"

QOgrePointLightPrivate::QOgrePointLightPrivate(Ogre::SceneManager * scene, Ogre::SceneNode * parentNode, QOgreLight * q)
	: QOgreLightPrivate(scene, parentNode, q), position(positionData)
{
	positionData.setNode(node);
}

QOgrePointLightPrivate::~QOgrePointLightPrivate()
{
	positionData.setNode(NULL);
}
