#ifndef QOGREENGINE_P_H
#define QOGREENGINE_P_H

#include "qogre.h"
#include "qogreobject_p.h"
#include "qogrepagemanager.h"

#include <QString>
#include <QSet>
#include <QPointer>

#include <OgreRoot.h>

class QOgreEngine;
class QOgreEnginePrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreEngine)

	static const QString PLUGINS_PREFIX;

	QOgreEnginePrivate(QOgreEngine * q);
	~QOgreEnginePrivate();

	void loadPlugins();
	void initialize();

	void select(Ogre::RenderSystem *, bool = false);

	void addResourceLocation(const QString &, QtOgre::ResourceType);
	bool loadAllResources();

	Ogre::Root * root;
	QSet<QString> loadedResources;
	bool resourcesInitialized;

	QSet<QString> requiredPlugins;
	QList<Ogre::Plugin *> plugins;
	QPointer<QOgrePageManager> pageManager;
};

#endif // QOGREENGINE_P_H
