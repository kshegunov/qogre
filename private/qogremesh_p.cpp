#include "qogremesh_p.h"
#include "qogremesh.h"
#include "qogrecast.h"

#include <OgreSceneManager.h>

QOgreMeshPrivate::QOgreMeshPrivate(const QString & file, QOgreScene * parent, QOgreMesh * q)
	: QOgreNodePrivate(parent, q), entity(NULL)
{
	create(qogre_cast(parent), file);
}

QOgreMeshPrivate::QOgreMeshPrivate(const QString & file, QOgreNode * parent, QOgreMesh * q)
	: QOgreNodePrivate(parent, q), entity(NULL)
{
	create(qogre_cast(parent)->getCreator(), file);
}

void QOgreMeshPrivate::create(Ogre::SceneManager * scene, const QString & file)
{
	entity = scene->createEntity(file.toStdString());
	node->attachObject(entity);

	entity->setCastShadows(true);
}

QOgreMeshPrivate::~QOgreMeshPrivate()
{
	if (!isValid())
		return;

	node->detachObject(entity->getName());
	node->getCreator()->destroyEntity(entity);
}
