#include "qogrecamera_p.h"
#include "qogrecamera.h"
#include "qogrecast.h"

#include <OgreSceneManager.h>

#include <QtMath>

QOgreCameraPrivate::QOgreCameraPrivate(QOgreScene * scene, QOgreCamera * q)
	: QOgreNodePrivate(scene, q), anchor(NULL)
{
	// Create the camera
	static qint32 cameraIndex = 0;
	camera = qogre_cast(scene)->createCamera(QString("QOgre/QOgreCamera#%1").arg(cameraIndex++).toStdString());
	node->attachObject(camera);
}

QOgreCameraPrivate::~QOgreCameraPrivate()
{
	delete detachAnchor();

	node->detachObject(camera);
	node->getCreator()->destroyCamera(camera);
}

void QOgreCameraPrivate::attachAnchor(QOgreNode * anchorNode)
{
	Ogre::SceneNode * ogreAnchor = qogre_cast(anchorNode);

	Q_ASSERT(ogreAnchor->getCreator() == node->getCreator());
	if (anchorNode == anchor)
		return;

	anchor = anchorNode;

	// Set up the camera
	node->getParent()->removeChild(node);
	ogreAnchor->addChild(node);
	camera->lookAt(ogreAnchor->getPosition());
}

QOgreNode * QOgreCameraPrivate::detachAnchor()
{
	QOgreNode * result = anchor;

	if (anchor)  {
		Ogre::SceneNode * ogreAnchor = qogre_cast(anchor);
		ogreAnchor->removeChild(node);
		ogreAnchor->getParent()->addChild(node);
	}

	return result;
}

void QOgreCameraPrivate::rotate(qreal angle, const QVector3D & axis)
{
	if (anchor)
		anchor->orientation().rotate(angle, axis);
	else
		node->rotate(qogre_cast(axis), Ogre::Degree(angle));
}
