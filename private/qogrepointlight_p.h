#ifndef QOGREPOINTLIGHT_P_H
#define QOGREPOINTLIGHT_P_H

#include "qogrelight_p.h"
#include "qogreposition.h"
#include "qogreposition_p.h"

class QOgrePointLight;
class QOGRE_LOCAL QOgrePointLightPrivate : public QOgreLightPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgrePointLight)

	QOgrePointLightPrivate(Ogre::SceneManager *, Ogre::SceneNode *, QOgreLight *);
	virtual ~QOgrePointLightPrivate();

	QOgrePositionPrivate positionData;
	QOgrePosition position;
};

#endif // QOGREPOINTLIGHT_P_H
