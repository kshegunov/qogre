#ifndef QOGRERENDERER_P_H
#define QOGRERENDERER_P_H

#include "qogrerenderer.h"
#include "qogreobject_p.h"

#include <QString>
#include <QHash>

#include <OgreRenderSystem.h>

typedef QPair<QString, QString> RendererOption;
typedef QList<RendererOption> RendererOptionList;

class QOgreEnginePrivate;

class QOgreRenderer;
class QOGRE_LOCAL QOgreRendererPrivate : public QOgreObjectPrivate
{
	friend class QOgreEnginePrivate;

	QOGRE_OBJECT_PRIVATE(QOgreRenderer)

	QOgreRendererPrivate(const QString &, QOgreRenderer *);
	virtual ~QOgreRendererPrivate();

	static void loadRenderers();
	static QOgreRenderer * current();

	void flushOptions();
	void select();

	const QString name;
	Ogre::RenderSystem * renderer;

	RenderOptions options;
	bool changed, selected;

	static QHash<QString, QOgreRenderer *> renderers;
	static QOgreRenderer * currentRenderer;
};

// ------------------------------------------------------------------------------------------------------------------------------------------------------ //

inline QOgreRenderer * QOgreRendererPrivate::current()
{
	return currentRenderer;
}

#endif // QOGRERENDERER_P_H
