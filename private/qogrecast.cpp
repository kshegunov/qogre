#include "qogrecast.h"
#include "qogrelog.h"
#include "qrcarchive.h"

const QOgreCast qogre_cast;

Ogre::Node::TransformSpace QOgreCast::operator () (PositionType position) const
{
	// TODO: Put in a table lookup
	if (position.testFlag(QtOgre::GlobalPosition))
		return Ogre::Node::TS_WORLD;
	else if (position.testFlag(QtOgre::LocalPosition))  {
		if (position.testFlag(QtOgre::AbsolutePosition))
			return Ogre::Node::TS_PARENT;
		else if (position.testFlag(QtOgre::RelativePosition))
			return Ogre::Node::TS_LOCAL;
	}

	QOgreLog::warning("Trying to change position but with an unknown pivot point.");		// Unknown type (put warning)
	return Ogre::Node::TS_PARENT;
}

QString QOgreCast::operator() (const QtOgre::ResourceType type) const
{
	Q_ASSERT(type >= QtOgre::FilesystemResource && type < QtOgre::UnknownResource);

	static QString ResourceTypes[] = { QString("FileSystem"), QString("Zip"), QString::fromStdString(Ogre::QRCArchive::ResourceType) };		// Initialize the resource types
	return ResourceTypes[type];
}

Ogre::SceneTypeMask QOgreCast::operator () (QOgreExteriorScene::SceneType type) const
{
	switch (type)
	{
	case QOgreExteriorScene::CloseScene:
		return Ogre::ST_EXTERIOR_CLOSE;
	case QOgreExteriorScene::FarScene:
		return Ogre::ST_EXTERIOR_FAR;
	case QOgreExteriorScene::VeryFarScene:
		return Ogre::ST_EXTERIOR_REAL_FAR;
	}

	QOgreLog::warning("Trying to create a scene with an unknown type.");		// Unknown type (put warning)
	return Ogre::ST_EXTERIOR_CLOSE;
}

Ogre::ShadowTechnique QOgreCast::operator () (const QtOgre::ShadowType type) const
{
	switch (type)
	{
	case QtOgre::ModulativeTextureShadow:
		return Ogre::SHADOWTYPE_TEXTURE_MODULATIVE;
	case QtOgre::ModulativeStencilShadow:
		return Ogre::SHADOWTYPE_STENCIL_MODULATIVE;
	case QtOgre::AdditiveStencilShadow:
		return Ogre::SHADOWTYPE_STENCIL_ADDITIVE;
	default:
		return Ogre::SHADOWTYPE_NONE;
	}
}

QtOgre::ShadowType QOgreCast::operator () (const Ogre::ShadowTechnique technique) const
{
	switch (technique)
	{
	case Ogre::SHADOWTYPE_TEXTURE_MODULATIVE:
		return QtOgre::ModulativeTextureShadow;
	case Ogre::SHADOWTYPE_STENCIL_MODULATIVE:
		return QtOgre::ModulativeStencilShadow;
	case Ogre::SHADOWTYPE_STENCIL_ADDITIVE:
		return QtOgre::AdditiveStencilShadow;
	default:
		return QtOgre::NoShadow;
	}
}

RendererOptionList QOgreCast::operator () (const RenderOptions options) const
{
	RendererOptionList result;
	result << RendererOption("Full Screen", options.testFlag(QtOgre::RenderFullScreen) ? "Yes" : "No")
		   << RendererOption("VSync", options.testFlag(QtOgre::RenderVSync) ? "Yes" : "No");

	return result;
}

Ogre::Capabilities QOgreCast::operator() (const QtOgre::RenderFeatures features) const
{
	switch (features)
	{
	case QtOgre::InfiniteFarPlaneFeature:
		return Ogre::RSC_INFINITE_FAR_PLANE;
	}

	return static_cast<Ogre::Capabilities>(0);
}

Ogre::Terrain::Alignment QOgreCast::operator() (const QtOgre::PrincipalPlanes alignment) const
{
	switch (alignment)
	{
	case QtOgre::XZPlane:
		return Ogre::Terrain::ALIGN_X_Z;
	case QtOgre::YZPlane:
		return Ogre::Terrain::ALIGN_Y_Z;
	case QtOgre::XYPlane:
	default:
		return Ogre::Terrain::ALIGN_X_Y;
	}
}
