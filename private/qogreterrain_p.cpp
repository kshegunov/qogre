#include "qogreterrain_p.h"
#include "qogreterrain.h"
#include "qogreengine.h"
#include "qogrecast.h"
#include "qogremath.h"

bool QOgreTerrainPrivate::PageProvider::prepareProceduralPage(Ogre::Page * page, Ogre::PagedWorldSection * section)
{
	Q_UNUSED(page);
	Q_UNUSED(section);
	return true;
}

bool QOgreTerrainPrivate::PageProvider::loadProceduralPage(Ogre::Page * page, Ogre::PagedWorldSection * section)
{
	Q_UNUSED(page);
	Q_UNUSED(section);
	return true;
}

bool QOgreTerrainPrivate::PageProvider::unloadProceduralPage(Ogre::Page * page, Ogre::PagedWorldSection * section)
{
	Q_UNUSED(page);
	Q_UNUSED(section);
	return true;
}

bool QOgreTerrainPrivate::PageProvider::unprepareProceduralPage(Ogre::Page * page, Ogre::PagedWorldSection * section)
{
	Q_UNUSED(page);
	Q_UNUSED(section);
	return true;
}

void QOgreTerrainPrivate::TerrainDefiner::define(Ogre::TerrainGroup * terrainGroup, long x, long y)
{
	terrainGroup->defineTerrain(x, y, 0.0f);
}

QOgreTerrainPrivate::TerrainDefiner::~TerrainDefiner()
{
}

QOgreTerrainPrivate::QOgreTerrainPrivate(QOgreTerrain * q, QOgreWorld * ogreWorld)
	: QOgreObjectPrivate(q), world(ogreWorld), alignment(QtOgre::XZPlane), terrainSize(1, 1), tileSize(2), worldSize(1), lod(1), light(NULL), pageSize(3, 3), terrainGroup(NULL), terrainPaging(NULL)
{
}

QOgreTerrainPrivate::~QOgreTerrainPrivate()
{
	if (terrainGroup)
		terrainGroup->removeAllTerrains();
	delete terrainPaging;
}

void QOgreTerrainPrivate::initialize()
{
	// Retrieve the scene manager
	Ogre::SceneManager * sceneManager = qogre_cast(world->scene());

	// Initialize global terrain options from the light source first
	Ogre::TerrainGlobalOptions * options = Ogre::TerrainGlobalOptions::getSingletonPtr();
	if (light)  {
		Ogre::Light * ogreLight = qogre_cast(light);
		options->setLightMapDirection(ogreLight->getDerivedDirection());
		options->setCompositeMapDiffuse(ogreLight->getDiffuseColour());
	}
	options->setCompositeMapAmbient(sceneManager->getAmbientLight());

	qDebug() << "Terrain group at: " << origin.x() << ' ' << origin.y() << ' ' << origin.z() << endl;

	// Calculate the number of vertices per terrain tile
	quint16 vertices = tileSize + 1;

	qDebug() << "Tile size: " << tileSize << ", Vertices: " << vertices << endl;

	// Prepare the default terrain data
	terrainGroup = new Ogre::TerrainGroup(sceneManager, qogre_cast(alignment), vertices, worldSize);
	terrainGroup->setOrigin(qogre_cast(origin));
	terrainGroup->setFilenameConvention("", "");

	Ogre::Terrain::ImportData & importData = terrainGroup->getDefaultImportSettings();
	importData.terrainSize = vertices;
	importData.inputScale = 1;
	importData.deleteInputData = false;
	importData.constantHeight = 0.0f;
	importData.inputFloat = NULL;
	// Calculate the batch size based on the requested levels of detail
	importData.maxBatchSize = qBound<quint16>(1, Ogre::Terrain::TERRAIN_MAX_BATCH_SIZE, vertices);
	importData.minBatchSize = (importData.maxBatchSize - 1) / (1 << (lod - 1)) + 1;

	// Add the layers
	qint32 layersNumber = layers.size();

	Ogre::Terrain::LayerInstanceList & ogreLayers = importData.layerList;
	ogreLayers.resize(layersNumber);

	for (qint32 i = 0; i < layersNumber; i++)  {
		const QOgreTerrainLayer & layer = layers.at(i);
		QStringList textures = layer.textures();

		// Fill up the data for this terrain layer
		Ogre::Terrain::LayerInstance & ogreLayer = ogreLayers[i];
		ogreLayer.worldSize = layer.worldSize();
		ogreLayer.textureNames.resize(textures.size());
		for (qint32 i = 0, size = textures.size(); i < size; i++)
			ogreLayer.textureNames[i] = textures.at(i).toStdString();
	}

	// Calculate the number of pages in each direction
	qreal loadRadius = qSqrt(qSqr(pageSize.width() * worldSize) + qSqr(pageSize.height() * worldSize));

	// Create and initialize the paging
	Ogre::PageManager * pageManager = qogre_cast(QOgreEngine::instance()->pageManager());
	pageManager->setPageProvider(&pageProvider);

	terrainPaging = new Ogre::TerrainPaging(pageManager);
	Ogre::TerrainPagedWorldSection * section = terrainPaging->createWorldSection(qogre_cast(world), terrainGroup, loadRadius, 3 * loadRadius, 0, 0, terrainSize.width(), terrainSize.height(), Ogre::StringUtil::BLANK, 0);

	section->setDefiner(new TerrainDefiner);

	terrainGroup->freeTemporaryResources();
}
