#include "qogreworld_p.h"
#include "qogreworld.h"
#include "qogrecast.h"

qint32 QOgreWorldPrivate::worldsIndex = 0;
QOgreWorldPrivate::QOgreWorldsHash QOgreWorldPrivate::worlds;

QOgreWorldPrivate::QOgreWorldPrivate(const QString & worldName, QOgreWorld * q)
	: QOgreObjectPrivate(q), name(worldName)
{
	QOgrePageManager * pageManager = QOgreEngine::instance()->pageManager();
	world = qogre_cast(pageManager)->createWorld();
}

QOgreWorldPrivate::~QOgreWorldPrivate()
{
	worlds.remove(name);

	QOgrePageManager * pageManager = QOgreEngine::instance()->pageManager();
	qogre_cast(pageManager)->destroyWorld(world);
}

void QOgreWorldPrivate::changeName(const QString & newName)
{
	QOgreWorldsHash::Iterator iterator = worlds.find(newName);
	Q_ASSERT_X(iterator == worlds.end(), "QOgreWorldPrivate::changeName()", "A world with that name already exists");

	worlds.remove(name);
	worlds.insert(newName, q());
	name = newName;
}

