#ifndef QOGRELIGHT_P_H
#define QOGRELIGHT_P_H

#include "qogreobject_p.h"

#include <OgreLight.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

class QOgreLight;
class QOGRE_LOCAL QOgreLightPrivate : public QOgreObjectPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreLight)

protected:
	QOgreLightPrivate(Ogre::SceneManager *, Ogre::SceneNode *, QOgreLight *);
	virtual ~QOgreLightPrivate();

	bool isValid() const;
	void createLight();
	void destroyLight();
	void createNode(Ogre::SceneNode *);
	void destroyNode();

	Ogre::Light * light;
	Ogre::SceneNode * node;
	Ogre::SceneManager * scene;
};

inline bool QOgreLightPrivate::isValid() const
{
	return light && node;
}

#endif // QOGRELIGHT_P_H
