#ifndef QOGRESPOTLIGHT_P_H
#define QOGRESPOTLIGHT_P_H

#include "qogrelight_p.h"
#include "qogreposition.h"
#include "qogreposition_p.h"
#include "qogreorientation.h"
#include "qogreorientation_p.h"

class QOgreSpotLight;
class QOGRE_LOCAL QOgreSpotLightPrivate : public QOgreLightPrivate
{
	QOGRE_OBJECT_PRIVATE(QOgreSpotLight)

	QOgreSpotLightPrivate(Ogre::SceneManager *, Ogre::SceneNode *, QOgreLight *);
	virtual ~QOgreSpotLightPrivate();

	QOgrePositionPrivate positionData;
	QOgrePosition position;
	QOgreOrientationPrivate orientationData;
	QOgreOrientation orientation;
};

#endif // QOGRESPOTLIGHT_P_H
