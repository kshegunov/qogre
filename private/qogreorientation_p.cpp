#include "qogreorientation_p.h"

QOgreOrientationPrivate::QOgreOrientationPrivate(Ogre::Node * ogreNode)
	: node(ogreNode)
{
}

void QOgreOrientationPrivate::setNode(Ogre::Node * ogreNode)
{
	node = ogreNode;
}
