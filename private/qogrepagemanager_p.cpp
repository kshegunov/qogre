#include "qogrepagemanager_p.h"
#include "qogrepagemanager.h"

QOgrePageManagerPrivate::QOgrePageManagerPrivate(QOgrePageManager * q)
	: QOgreObjectPrivate(q), pageManager(new Ogre::PageManager)
{
}

QOgrePageManagerPrivate::~QOgrePageManagerPrivate()
{
	delete pageManager;
}

