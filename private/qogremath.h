#ifndef QOGREMATH_H
#define QOGREMATH_H

#include <QtMath>

template <class T>
T qSqr(const T x)
{
	return x * x;
}

template <class T>
bool qIsPowerOfTwo(const T x)
{
	return (x & (x - 1)) == 0 && x != 0;
}

#endif // QOGREMATH_H

