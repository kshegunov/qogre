#ifndef QOGREMESH_H
#define QOGREMESH_H

#include "qogrenode.h"
#include "qogreworld.h"

class QOgreScene;
class QOgreEnvelope;
class QOgreMeshPrivate;

//! The QOgreMesh class encapsulates a mesh resource.
//! The meshes are loaded when needed from the registered resources using the supplied file name.
//! They are attached either to a scene (a scene's root node), or to an another mesh. When attached
//! to an another mesh the transformations applied to the parent are automatically propagated to the
//! children.
class QOGRE_API QOgreMesh : public QOgreNode
{
	Q_OBJECT
	QOGRE_DISABLE_COPY(QOgreMesh)
	QOGRE_OBJECT(QOgreMesh)

public:
	//! The mesh constructor.
	//! Creates the object and tries to load the mesh from the specified location. The file path is
	//! deduced from one of the registered resource locations.
	//! \param file The location for the mesh file.
	//! \param world The world to which the mesh should be added.
	QOgreMesh(const QString & file, QOgreWorld * world = QOgreWorld::world());

	//! The mesh constructor.
	//! Creates the object and tries to load the mesh from the specified location. The file path is
	//! deduced from one of the registered resource locations.
	//! \param file The location for the mesh file.
	//! \param scene The scene to which the mesh should be added.
	QOgreMesh(const QString & file, QOgreScene * scene);

	//! The mesh constructor.
	//! Creates the object and tries to load the mesh from the specified location. The file path is
	//! deduced from one of the registered resource locations. The mesh is attached to the specified
	//! parent. Any orientation and position transformations applied to the parent will be
	//! propagated to the current object.
	//! \param file The location for the mesh file.
	//! \param parent The parent node.
	QOgreMesh(const QString & file, QOgreNode * parent);

	virtual ~QOgreMesh();

	//! Checks if the mesh was created and properly initialized.
	//! \return Whether the mesh is valid.
	bool isValid() const;

	//! Gets the mesh's envelope.
	//! \return The mesh's envelope.
	QOgreEnvelope envelope() const;


};

#endif // QOGREMESH_H
