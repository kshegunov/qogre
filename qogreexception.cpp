#include "qogreexception.h"

QOgreException::QOgreException(const QString & description)
	: text(description)
{
}

const QString & QOgreException::description() const throw()
{
	return text;
}

QOgreInitializationException::QOgreInitializationException(const QString & text)
	: QOgreException(text)
{
}
