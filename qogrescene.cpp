#include "qogrescene.h"
#include "qogrecast.h"
#include "qogrelog.h"
#include "qogrescene_p.h"
#include "qogreengine.h"
#include "qogreworld.h"

#include <OgreSceneManager.h>

#include <QHash>
#include <QAtomicInt>

QOgreScene::QOgreScene(QOgreWorld * world, QOgreScenePrivate & d)
	: QOgreObject(d, world)
{
	QOgreEngine * ogre = QOgreEngine::instance();
	QObject::connect(ogre, SIGNAL(ready()), this, SLOT(prepare()));

	world->setScene(this);
}

QOgreWorld * QOgreScene::world()
{
	return qobject_cast<QOgreWorld *>(parent());
}

const QOgreWorld * QOgreScene::world() const
{
	return qobject_cast<QOgreWorld *>(parent());
}

bool QOgreScene::isValid() const
{
	return d()->scene;
}

void QOgreScene::prepare()
{
	emit ready(this);
}

void QOgreScene::setShadowType(ShadowType type)
{
	Q_ASSERT(type >= 0 && type <= QtOgre::NoShadow);
	d()->scene->setShadowTechnique(qogre_cast(type));
}

QOgreScene::ShadowType QOgreScene::shadowType() const
{
	return qogre_cast(d()->scene->getShadowTechnique());
}

void QOgreScene::setAmbientLightColor(const QColor & color)
{
	d()->scene->setAmbientLight(qogre_cast(color));
}

QColor QOgreScene::ambientLightColor() const
{
	return qogre_cast(d()->scene->getAmbientLight());
}

QOgreGenericScene::QOgreGenericScene(QOgreWorld * world)
	: QOgreScene(world, *new QOgreScenePrivate(Ogre::ST_GENERIC, this))
{
}

QOgreInteriorScene::QOgreInteriorScene(QOgreWorld * world)
	: QOgreScene(world, *new QOgreScenePrivate(Ogre::ST_INTERIOR, this))
{
}

QOgreExteriorScene::QOgreExteriorScene(QOgreWorld * world, SceneType type)
: QOgreScene(world, *new QOgreScenePrivate(qogre_cast(type), this))
{
}
