#include "qogreworld.h"
#include "qogreworld_p.h"
#include "qogreengine.h"
#include "qogrecast.h"
#include "qogrescene.h"
#include "qogreterrain.h"

QOgreWorld::QOgreWorld(const QString & name)
	: QOgreObject(*new QOgreWorldPrivate(name, this), QOgreEngine::instance()->pageManager())
{
	QOGRE_D(QOgreWorld);
	d->changeName(name);
}

QOgreWorld::~QOgreWorld()
{
}

void QOgreWorld::setScene(QOgreScene * scene)
{
	d()->scene = scene;
}

QOgreScene * QOgreWorld::scene()
{
	return d()->scene;
}

const QOgreScene * QOgreWorld::scene() const
{
	return d()->scene;
}

void QOgreWorld::setTerrain(QOgreTerrain * terrain)
{
	d()->terrain = terrain;
}

QOgreTerrain * QOgreWorld::terrain()
{
	return d()->terrain;
}

const QOgreTerrain * QOgreWorld::terrain() const
{
	return d()->terrain;
}

void QOgreWorld::setName(const QString & name)
{
	d()->changeName(name);
}

QString QOgreWorld::name() const
{
	return d()->name;
}

QOgreWorld * QOgreWorld::world(const QString & name)
{
	QOgreWorldPrivate::QOgreWorldsHash::Iterator iterator = QOgreWorldPrivate::worlds.find(name);
	return (iterator != QOgreWorldPrivate::worlds.end()) ? iterator.value() : NULL;
}
